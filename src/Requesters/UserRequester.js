let Requester = require('./Requester');
let requestPromise = require('request-promise-native');

/**
 * UserRequester make requests in order to create a new User
 */
class UserRequester extends Requester {
    constructor() {
        super();
        this.options.uri = this.options.uri + '/users';
        this.options.json = true;
    }

    /**
     * Asks the server to create a new User using usename/password login
     * @param username the new User's username
     * @param password the new User's password
     * @return {Promise}
     */
    add(username, password) {
        let options = Object.assign(this.options, {
            method: "POST",
            body: {username, password}
        });

        return requestPromise(options)
            .catch(res => Promise.reject(res.error));
    }
}

module.exports = UserRequester;

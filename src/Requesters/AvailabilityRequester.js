'use strict';
const winston = require('winston');

let requestPromise = require('request-promise-native');

const Requester = require('./Requester');

/**
 * AvailabilityRequester makes a simple request
 * in order to verify the server is still reachable.
 */
class AvailabilityRequester extends Requester {
    constructor() {
        super();
        this.options.uri = this.options.uri + '/availability';
        this.options.method = 'GET';
    }

    /**
     * Tests the server availability
     * @return {Promise}
     */
    isServerAvailable() {
        return requestPromise(this.options)
            .then(() => {
                return true;
            })
            .catch(() => {
                return false;
            });
    }
}

module.exports = AvailabilityRequester;
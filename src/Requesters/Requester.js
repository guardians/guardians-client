let config = require('config');

/**
 * A Requester manages HTTPS request to server.
 * It make sure the credentials are correctly sent under each request,
 * and manage request results & errors.
 */
class Requester {
    constructor() {

        this.options = {
            uri: config.get('server.url'),
            headers: {
                username: config.get('credentials.username'),
                password: config.get('credentials.password')
            },
            strictSSL: config.get('server.strictSSL')
        };
    }
}

module.exports = Requester;
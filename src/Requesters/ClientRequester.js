let Requester = require('./Requester');
let requestPromise = require('request-promise-native');

/**
 * ClientRequester make requests in order to add a particular Client for a machine
 */
class ClientRequester extends Requester {
    constructor() {
        super();
        this.options.uri = this.options.uri + '/clients';
        this.options.json = true;
    }

    /**
     * Asks the server to create a new Client using usename/password login
     * @param username the new Client's username
     * @param password the new Client's password
     * @param user the User associated to the new Client
     * @return {Promise}
     */
    add(username, password, user) {
        let options = Object.assign(this.options, {
            headers: user,
            method: "POST",
            body: {username, password}
        });

        return requestPromise(options)
            .catch(res => Promise.reject(res.error));
    }
}

module.exports = ClientRequester;

'use strict';

let fs = require('fs');
let request = require('request');
let requestPromise = require('request-promise-native');
const config = require('config');

let Utils = require("../Utils");
const Requester = require('./Requester');

/**
 * FileRequester manages paths synchronisation between Client and Server.
 * It makes a request for each path add/update/remove,
 * and also asks for restoring a path.
 */
class FileRequester extends Requester {
    constructor() {
        super();

        this.options = Object.assign(this.options, {
            uri: this.options.uri + '/paths',
            json: true
        });
    }

    /**
     * Adds a new file to server
     * @param filepath the path of the file to add
     * @return {Promise}
     */
    addFile(filepath) {
        let options = this.createFileData(filepath);
        options.method = 'PUT';
        return requestPromise(options)
            .catch(res => Promise.reject(res.error));
    }

    /**
     * Updates an existing file on server
     * @param filepath the path of the file to update
     * @return {Promise}
     */
    updateFile(filepath) {
        let options = this.createFileData(filepath);
        options.method = 'POST';

        return requestPromise(options)
            .catch(res => Promise.reject(res.error));
    }

    /**
     * Create a new version of an existing file on server
     *
     * @param filepath {string} - the path of the file to update
     * @param keep {number|null} - the number of versions to keep
     * @return {Promise}
     */
    createVersion(filepath, keep = null) {
        let options = this.createFileData(filepath);
        options.method = 'POST';
        options.uri = options.uri + '/versionize';

        if (keep) {
            options.body = {
                keep: keep
            };
        }

        return requestPromise(options)
            .catch(res => Promise.reject(res.error));
    }

    /**
     * Remove a path from server
     * @param filepath the path of the file to remove
     * @return {Promise}
     */
    removePath(filepath) {
        let options = Object.assign({}, this.options, {
            method: 'DELETE',
            body: {
                path: Utils.systemToPosix(filepath)
            }
        });

        return requestPromise(options)
            .catch(res => Promise.reject(res.error));
    }

    /**
     * Restore a path from server
     * @param filepath {string} the path to restore
     * @param version {Number} the version tu restore
     * @return {*}
     */
    restore(filepath, version = null) {
        let posixFilepath = Utils.systemToPosix(filepath);

        let uri = [
            this.options.uri,
            'download'
        ];
        if (version !== null) {
            uri.push('version');
            uri.push(version.toString())
        }
        uri.push(encodeURIComponent(posixFilepath));

        let options = Object.assign({}, this.options, {
            uri: uri.join('/'),
            method: 'GET',
            resolveWithFullResponse: true,
            json: false
        });

        return request(options);
    }

    /**
     * Creates an object which contains the content of the provided file path
     *
     * @private
     * @param filepath string - the path corresponding to the file to send
     * @returns Object
     */
    createFileData(filepath) {
        let formData = {
            path: Utils.systemToPosix(filepath),
            file: fs.createReadStream(filepath)
        };

        return Object.assign({}, this.options, {
            formData: formData,
        });
    }
}

module.exports = FileRequester;

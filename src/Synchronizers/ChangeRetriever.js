let fs = require('q-io/fs');
const path = require('path');

const WatchedSeeker = require('../Seekers/WatchedSeeker');
const Manager = require('../Manager');
const AddedSeeker = require('../Seekers/AddedSeeker');

/**
 * ChangeRetriever looks for watched path's changes which append when the Client wasn't started up.
 * It in charge to find which files has been added/updated/removed,
 * and communicates every changes to the Manager,
 * in order to synchronise theses with the Server
 */
class ChangeRetriever {
    constructor(watchedSeeker = new WatchedSeeker(),
                addedSeeker = new AddedSeeker(),
                manager = new Manager(watchedSeeker, addedSeeker)) {
        this.watchedSeeker = watchedSeeker;
        this.addedSeeker = addedSeeker;
        this.manager = manager;
    }

    /**
     * Find for file changes
     * @return {Promise}
     */
    start() {
        return Promise.all([
            this.findChanged(),
            this.findRemoved()
        ]);
    }

    /**
     * Find paths which has been created/updated since the last run
     *
     * @private
     * @return {Promise}
     */
    findChanged() {
        return this.watchedSeeker.all()
            .then(allWatched => {
                let promises = Object.keys(allWatched)
                    .map(watchedPath => {
                        return this.checkForChanges(watchedPath, allWatched[watchedPath])
                    });

                return Promise.all(promises);
            });
    }

    /**
     * Find file change for watchedPath
     *
     * @private
     * @param watchedPath - the path to checkForChanges
     * @param options - options for the path to checkForChanges
     * @return {Promise}
     */
    checkForChanges(watchedPath, options) {
        return Promise.all([
            this.addedSeeker.findOptions(watchedPath),
            fs.stat(watchedPath)
        ])
            .then(results => {
                let [addedOptions, stat] = results;

                // FIX: q-io/fs don't provide stat.mtime
                // We create mtime property that will be read be addedSeeker
                stat.mtime = stat.lastModified();

                if (stat.isDirectory()) {
                    if (options.recursive) {
                        return this.checkDirectory(watchedPath, options);
                    }
                }
                else if (!addedOptions) {
                    return this.manager.addFile(watchedPath, stat);
                }
                else if (addedOptions.mtime < stat.mtime) {
                    return this.manager.updateFile(watchedPath, stat);
                }

                return Promise.resolve();
            });
    }

    /**
     * Recursively checkForChanges the content of a directory
     *
     * @private
     * @param watchedPath - the path of the directory to checkForChanges
     * @param options - the options of the path to checkForChanges
     * @return {Promise}
     */
    checkDirectory(watchedPath, options) {
        return fs.list(watchedPath)
            .then(res => {
                let allPromises = res.map(item => this.checkForChanges(path.join(watchedPath, item), options));
                return Promise.all(allPromises);
            });
    }

    /**
     * Find paths which no longer exist
     * For each file/folder found, call unlink/unlinkDir
     *
     * @private
     * @return {Promise}
     */
    findRemoved() {
        return this.addedSeeker.allHierarchically()
            .then(allWatched => {
                let allPromises = Object.keys(allWatched)
                    .filter(key => key !== '__options')
                    .map(watchedPath => {
                        // FIX: If watchedPath is a UNIX path,
                        // Make the watchedPath absolute again.
                        let absolutePath = path.sep === '/' ? '/' + watchedPath : watchedPath;

                        return this.recursiveFindRemoved(absolutePath, allWatched[watchedPath]);
                    });

                return Promise.all(allPromises);
            });
    }

    /**
     * Recursively checked
     *
     * @private
     * @param watchedPath
     * @param hierarchy
     * @return {Promise}
     */
    recursiveFindRemoved(watchedPath, hierarchy) {
        return Promise.all([
            fs.exists(watchedPath),
            fs.isDirectory(watchedPath)
        ])
            .then(results => {
                let [exists, isDirectory] = results;

                if (!exists) {
                    if (isDirectory) {
                        return this.manager.removeFolder(watchedPath);
                    }
                    return this.manager.removePath(watchedPath);
                }

                else if (isDirectory) {
                    let allPromises = Object.keys(hierarchy)
                        .filter(key => key !== '__options')
                        .map(nextSubPath => {
                            let absoluteNextSubPath = path.join(watchedPath, nextSubPath);
                            return this.recursiveFindRemoved(absoluteNextSubPath, hierarchy[nextSubPath]);
                        });

                    return Promise.all(allPromises);
                }
            });
    }
}

module.exports = ChangeRetriever;
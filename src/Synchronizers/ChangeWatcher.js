'use strict';

const EventEmitter = require('events');
const config = require('config');
let chokidar = require('chokidar');

const Manager = require('../Manager');
const WatchedSeeker = require('../Seekers/WatchedSeeker');

/**
 * ChangeRetriever looks for watched path's changes which append when the Client is up.
 * It adds a Chokidar watcher on each path to watch,
 * and handle changes by communicating to the Manager,
 * in order to synchronise theses with the Server
 */
class ChangeWatcher extends EventEmitter {

    /**
     *
     * @param manager Manager
     * @param watchedSeeker WatchedSeeker
     */
    constructor(watchedSeeker = new WatchedSeeker(),
                manager = new Manager(watchedSeeker)) {
        super();

        this.manager = manager;
        this.watchedSeeker = watchedSeeker;
    }

    /**
     * Starts watching files & folders registered by WatchedSeeker
     */
    start() {
        if (this.watched) {
            this.stop();
        }

        return this.watchedSeeker.all()
            .then(watched => {
                this.watched = Object.keys(watched)
                    .map(filePath => this.watch(filePath, ChangeWatcher.getOptions(watched[filePath])));
            });
    }

    /**
     * Stops watching files & folders registered by WatchedSeeker
     */
    stop() {
        if (!this.watched) {
            return;
        }

        this.watched.forEach(watched => {
            watched.unwatch('*');
            watched.close();
        });
        delete this.watched;
    }

    /**
     * watch a file or folder, with provided options
     *
     * @private
     * @param {string} path - the file/folder path
     * @param {object} options - chokidar options
     * @return {object} chokidar watcher
     */
    watch(path, options) {
        return chokidar.watch(path, options)
            .on('add', (path, stats) => this.onAddFileDetected(path, stats))
            .on('change', (path, stats) => this.onChangeFileDetected(path, stats))
            .on('unlink', path => this.onUnlinkDetected(path))
            .on('unlinkDir', path => this.onUnlinkDirDetected(path));
    }


    /**
     * Handle `add` event
     *
     * @private
     * @param {string} path - the path of the file to handle
     * @param {fs.Stats} stats - file's stats
     */
    onAddFileDetected(path, stats) {
        return this.manager.addFile(path, stats)
            .catch(err => this.emit('error', err));
    }

    /**
     * Handles `change` event
     *
     * @private
     * @param {string} path - the path of the file to handle
     * @param {fs.Stats} stats - file's stats
     */
    onChangeFileDetected(path, stats) {
        return this.manager.updateFile(path, stats)
            .catch(err => this.emit('error', err));
    }

    /**
     * Handles `unlink` event
     *
     * @private
     * @param path string - the path of the file to handle
     */
    onUnlinkDetected(path) {
        return this.manager.removePath(path)
            .catch(err => this.emit('error', err));
    }

    /**
     * Handles `unlinkDir` event
     *
     * @private
     * @param path string - the path of the folder to handle
     */
    onUnlinkDirDetected(path) {
        return this.manager.removeFolder(path)
            .catch(err => this.emit('error', err));
    }

    static getOptions(options) {
        return Object.assign(config.get('chokidar.defaultOptions'), options)
    }
}

module.exports = ChangeWatcher;

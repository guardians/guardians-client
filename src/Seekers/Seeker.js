const path = require('path');
const Datastore = require('nedb');

/**
 * Seeker stores information about paths states.
 * Seeker is in charge to stores & gets every information others have to know in order to work.
 */
class Seeker {
    constructor(filepath) {
        this.datastore = new Datastore({
            filename: filepath,
            autoload: true
        });
    }

    /**
     * Get all stored objects
     *
     * @return {Promise} stored data
     */
    all() {
        return new Promise((resolve, reject) => {
            this.datastore.find(
                {},
                (err, res) => {
                    if (err) {
                        return reject(err);
                    }

                    let result = {};
                    res.forEach(item => {
                        result[item._id] = Seeker.documentToOptions(item);
                    });

                    return resolve(result);
                });
        });
    }

    allHierarchically() {
        return this.all()
            .then(allSeeked => {
                let hierarchy = {};

                Object.keys(allSeeked)
                    .forEach(seekedItemPath => {
                        let options = allSeeked[seekedItemPath];
                        return this.addToHierarchy(seekedItemPath, options, hierarchy);
                    });

                return hierarchy;
            });
    }

    addToHierarchy(seekedPath, seekedOptions, hierarchy) {
        let last = seekedPath
            .split(path.sep)
            .filter(item => !!item)
            .reduce((hierarchy, item) => {
                if (!hierarchy[item]) {
                    hierarchy[item] = {};
                }

                return hierarchy[item];
            }, hierarchy);

        last.__options = seekedOptions;
    }

    static documentToOptions(item) {
        if (!item) {
            return null;
        }

        let resultItem = {};
        Object.keys(item)
            .filter(key => key !== '_id')
            .forEach(key => resultItem[key] = item[key]);

        return resultItem;
    }
}

module.exports = Seeker;
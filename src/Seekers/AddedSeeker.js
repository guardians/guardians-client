'use strict';

const path = require('path');
const fs = require('q-io/fs');
const config = require('config');
const Seeker = require('./Seeker');

/**
 * AddedSeeker stores every paths that has already been added to the server
 */
class AddedSeeker extends Seeker {

    constructor(addedFile = config.get('seeker.added')) {
        super(addedFile);
    }

    /**
     * Retrieve added options corresponding to the provided path
     *
     * @param {string} path - the absolute path
     * @return {object} options corresponding to the provided path, or null if the path is not part of the added object.
     */
    findOptions(path) {
        return new Promise((resolve, reject) => {
            this.datastore.findOne({
                    _id: path
                },
                (err, res) => {
                    if (err) {
                        reject(err);
                    }

                    resolve(Seeker.documentToOptions(res));
                });
        });
    }

    /**
     * Remove a filepath from added ones
     *
     * @param {string} filepath - the filepath to remove
     * @throw Exception if filepath is not added
     * @return {Promise}
     */
    remove(filepath) {
        return new Promise((resolve, reject) => {
            this.datastore.remove(
                {
                    $or: [
                        {_id: filepath},
                        {_id: {$regex: new RegExp("^" + RegExp.escape(filepath + path.sep) + ".*")}}
                    ]
                },
                {multi: true},
                (err, numRemoved) => {
                    if (err) {
                        return reject(err);
                    }

                    if (numRemoved === 0) {
                        return reject(`Unable to remove ${filepath} because it doesn't exist`);
                    }

                    resolve(numRemoved);
                })
        });
    }

    /**
     * Check if the file/folder pointed by the provided path has already been added
     *
     * @param {string} path - the path of the file/folder to checkForChanges
     * @return {boolean} true only if this is the first time we handle this file
     */
    isNew(path) {
        return this.findOptions(path)
            .then(options => !options);
    }

    /**
     * Mark the provided path as added
     *
     * @param {string} path
     * @param {fs.Stats} stats
     * @return {Promise}
     */
    add(path, stats) {
        let document = AddedSeeker.createDocumentFromPath(path, stats);

        return new Promise((resolve, reject) => {
            this.datastore.insert(
                document,
                (err, res) => err ? reject(err) : resolve(res)
            );
        });
    }

    /**
     *
     * @private
     * @param path
     * @param stats
     * @returns Object
     */
    static createDocumentFromPath(path, stats) {
        return {
            _id: path,
            mtime: stats.mtime,
            isDirectory: stats.isDirectory()
        };
    }

    /**
     * Refresh the timestamp for the provided path
     *
     * @param path
     * @param stats
     * @returns {Promise}
     */
    update(path, stats) {
        let document = AddedSeeker.createDocumentFromPath(path, stats);
        return new Promise((resolve, reject) => {
            this.datastore.update(
                {_id: path},
                document,
                (err, nbUpdated) => {
                    if (err) {
                        return reject(err);
                    }
                    if (nbUpdated === 0) {
                        return reject(`Unable to update ${path} because it doesn't exist`);
                    }

                    resolve(nbUpdated);
                });
        });
    }
}

module.exports = AddedSeeker;

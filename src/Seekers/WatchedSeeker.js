'use strict';

const path = require('path');
const fs = require('q-io/fs');
const config = require('config');

const Seeker = require('./Seeker');

/**
 * WatchedSeeker manages paths to watch.
 */
class WatchedSeeker extends Seeker {
    /**
     *
     * @param {string} watchFile - the file containing watched data
     */
    constructor(watchFile = config.get('seeker.watched')) {
        super(watchFile);
    }

    /**
     * List all watched path
     *
     * @return {Promise} watched paths
     */
    list() {
        return new Promise((resolve, reject) => {
            this.datastore.find(
                {},
                ['_id'],
                (err, res) => {
                    if (err) {
                        reject(err);
                    }

                    resolve(res.map(item => item._id));
                });
        });
    }

    /**
     * Retrieves options associated to a filepath
     * Recursively traverse the filepath in order to find the most specific options.
     * @param {string} filepath
     */
    findOptions(filepath) {
        let splittedPath = fs.split(filepath);

        // FIXME: This will make several DB calls.
        // Improvement: make only one DB call which returns all needed data
        let allPromises = splittedPath.map((item, index) => {
            let arrayPath = splittedPath.slice(0, index + 1);
            let testedPath = arrayPath.join(path.sep);
            return this._retrieveOptions(testedPath);
        });

        // Merge each found result options
        // More the filepath is next to the provided filepath,
        // more its options are prioritized
        return Promise.all(allPromises)
            .then(results => {
                results = results.filter(item => item !== null);
                return Object.assign({}, ...results);
            });
    }

    /**
     * Try to find options for a particular path
     *
     * @private
     * @param path
     * @returns {Promise} resolved as soon as the DB returns the value
     * This value can be null is the provided path is not watched
     */
    _retrieveOptions(path) {
        return new Promise((resolve, reject) => {
            this.datastore.findOne(
                {_id: path},
                (err, res) => {
                    if (err) {
                        return reject(err);
                    }
                    if (!res) {
                        return resolve(null);
                    }
                    return resolve(WatchedSeeker.documentToOptions(res));
                });
        });
    }

    /**
     * Add a path to watched files & folders
     *
     * @param {string} path - the path to add to watched
     * @param {object} options - chokidar options
     * @return {Promise} the configuration write Promise.
     */
    add(path, options) {
        return new Promise((resolve, reject) => {
            WatchedSeeker.defaultPathOptions(path)
                .then(defaultOptions => {
                    let document = Object.assign({_id: path}, defaultOptions, options);
                    this.datastore.insert(
                        document,
                        (err, res) => err ? reject(err) : resolve(res));
                });
        });
    }

    /**
     * Remove a path from watched files & folders
     *
     * @param {string} path - the path to add to watched
     * @return {Promise} the configuration write Promise.
     */
    remove(path) {
        return new Promise((resolve, reject) => {
            this.datastore.remove(
                {_id: path},
                {multi: true},
                (err, numRemoved) => err ? reject(err) : resolve(numRemoved));
        });
    }

    /**
     * Update options of the provided path
     *
     * @param {string} path - the watched path to updated
     * @param {object} options - chokidar new options
     * @return {Promise} the configuration write Promise.
     */
    update(path, options) {
        return new Promise((resolve, reject) => {
            this.datastore.update(
                {_id: path},
                options,
                (err, res) => err ? reject(path + " is not watched yet") : resolve());
        });
    }

    /**
     * Compute default options for the provided file/folder path.
     * Check is the provided path is a file or a folder to determine the default options
     *
     * @private
     * @param {string} path
     * @return {Promise}
     */
    static defaultPathOptions(path) {
        return fs.isDirectory(path)
            .then(isDirectory => {
                return {
                    recursive: isDirectory
                };
            });
    }
}

module.exports = WatchedSeeker;

'use strict';

const path = require('path');
const Seeker = require('./Seeker');

/**
 * RemovedSeeker stores every path that has been (or will be) removed from the server.
 * It only purpose is to avoid unnecessary remove requests,
 * like concurrently removing a folder and its content.
 * Note that RemovedSeeker does't persist any data.
 */
class RemovedSeeker extends Seeker {

    constructor() {
        super(null);
    }

    /**
     * Mark the provided filepath as removed
     *
     * @param {string} filepath - the filepath to remove
     * @throw Exception if filepath is not added
     * @return {Promise}
     */
    markAsRemoved(filepath) {
        return new Promise((resolve, reject) => {
            this.datastore.insert(
                {
                    _id: filepath
                },
                (err, res) => err ? reject(err) : resolve(res));
        });
    }

    /**
     * Check if the provided filepath has already been removed.
     * Check if each parent of the provided filepath has been removed.
     * If any of all parents is removed, returns true, returns false otherwise.
     *
     * @param filepath - the file path to checkForChanges
     * @return {Promise} - Resolve returning true if the filepath or any of its parent is removed,
     * false otherwise
     */
    isRemoved(filepath) {
        return new Promise((resolve, reject) => {
            let splittedFilepath = filepath.split(path.sep);
            let pathsToFind = splittedFilepath.map((item, index) => {
                return {
                    _id: splittedFilepath.slice(0, index + 1).join(path.sep)
                };
            });

            this.datastore.findOne(
                {$or: pathsToFind},
                ['_id'],
                (err, res) => {
                    if (err) {
                        return reject(err);
                    }

                    return resolve(res !== null);
                });
        })
            .catch(err => {
                throw err;
            });
    }
}

module.exports = RemovedSeeker;

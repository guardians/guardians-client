const winston = require('winston');
const config = require('config');

let ChangeWatcher = require('./Synchronizers/ChangeWatcher');
let ChangeRetriever = require('./Synchronizers/ChangeRetriever');
let WatchedSeeker = require('./Seekers/WatchedSeeker');
let AddedSeeker = require('./Seekers/AddedSeeker');
let AvailabilityRequester = require('./Requesters/AvailabilityRequester');
let Manager = require('./Manager');
let Cli = require('./Cli');

const serverAvailibilityDelay = config.get('server.availabilityTest') * 1000;

/**
 * Guardian is the startup class of the Guardian-Client.
 * It manages class dependencies by creating a singleton for each necessary class.
 *
 * It also runs Watchers and handles their errors.
 *
 * In particular, Guardian verify the server availability in case of error,
 * and pause Watchers until the server is up again
 */
class Guardian {
    constructor() {
        this.addedSeeker = new AddedSeeker();
        this.watchedSeeker = new WatchedSeeker();
        this.manager = new Manager(this.watchedSeeker, this.addedSeeker);
        this.changeWatcher = new ChangeWatcher(this.watchedSeeker, this.manager);
        this.changeRetriever = new ChangeRetriever(this.watchedSeeker, this.addedSeeker, this.manager);
        this.availabilityRequester = new AvailabilityRequester();
    }

    /**
     * Run the cli system, so the user can configure the client.
     */
    cli() {
        new Cli(this.watchedSeeker, this.manager)
            .listen();
    }

    /**
     * Start watchers and handle server faults.
     * @return {*}
     */
    start() {
        this.changeWatcher.once('error', err => {
            winston.log('error', err);
            this.manageServerFault(err)
        });

        return this.startSync();
    }

    /**
     * Starts/Restarts watchers
     * @private
     * @return {Promise}
     */
    startSync() {
        return this.changeRetriever.start()
            .then(() => this.changeWatcher.start())
            .catch(err => this.manageServerFault(err));
    }

    /**
     * Manages server faults, by pausing whatchers,
     * and restarts them as soon as the server is up again.
     * @param error
     */
    manageServerFault(error) {
        winston.log('info', `server fault detected: ${error}`);

        this.changeWatcher.stop();

        if (error.code && error.code === 'ECONNREFUSED') {
            this.testServer();
        }
    }

    /**
     * Tests if the server is available or not.
     */
    testServer() {
        this.availabilityRequester.isServerAvailable()
            .then(isAvailable => {
                if (isAvailable) {
                    this.start();
                } else {
                    setTimeout(this.testServer.bind(this), serverAvailibilityDelay);
                }
            });
    }
}

module.exports = Guardian;

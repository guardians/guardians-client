'use strict';

const readline = require('readline');
const commander = require('commander');
const WatchedSeeker = require('./Seekers/WatchedSeeker');
const Manager = require('./Manager');

class Cli {
    constructor(watchedSeeker = new WatchedSeeker(), manager = new Manager()) {
        this.rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        this.watchedSeeker = watchedSeeker;
        this.manager = manager;

        commander
            .version('0.1.0');

        commander
            .command('list')
            .description('List all watched paths')
            .action(() => this.runList());

        commander
            .command('add <path>')
            .option('-c, --create', 'Add flag')
            .option('-u, --update', 'Update flag')
            .option('-d, --delete', 'Delete flag')
            .option('-r, --recursive', 'Recursive flag')
            .option('-v, --versionize', 'Versionize flag')
            .option('-k, --keep <keep>', 'Number of versions to keep', parseInt)
            .description('Mark a path as watched')
            .action((path, options) => this.runAdd(path, options));

        commander
            .command('update <path>')
            .option('-c, --create', 'Add flag')
            .option('-u, --update', 'Update flag')
            .option('-d, --delete', 'Delete flag')
            .option('-r, --recursive', 'Recursive flag')
            .option('-v, --versionized', 'Versionize flag')
            .option('-k, --keep <keep>', 'Number of versions to keep', parseInt)
            .description("Update a watched path's options")
            .action((path, options) => this.runUpdate(path, options));

        commander
            .command('rm <path>')
            .description('Remove a path from watched ones')
            .action(path => this.runRemove(path));

        commander
            .command('restore <path>')
            .description('Download a path from server')
            .option('-v, --versionized <version_number>', 'Version number to restore', parseInt)
            .action((path, version) => this.runRestore(path, version));

        commander
            .command("help")
            .alias("usage")
            .action(() => this.runHelp());

        commander
            .command("*")
            .arguments('<cmd>')
            .action(cmd => this.unknownCommand(cmd));
    }

    /**
     * Listen command line and run the associated command
     */
    listen() {
        this.rl.setPrompt(' > ');
        this.rl.on('line', this.runCommand.bind(this));
        this.rl.on('close', this.rl.close);
        this.rl.prompt();
    }

    /**
     * Run a command
     *
     * @param {Array} line - line enterred by user
     * @return Command
     */
    runCommand(line) {
        let argv = line.split(" ");
        argv.unshift('fake');
        argv.unshift(process.argv0);
        return commander.parse(argv);
    }

    /**
     * Manage a promise by displaying results or errors
     *
     * @private
     * @param promise {Promise} - the promise to manage
     * @return {Promise} - the promise
     */
    managePromise(promise) {
        return promise
            .then((response) => {
                if (response) {
                    console.log(response);
                }
                this.rl.prompt();
            })
            .catch((error) => {
                console.error(error);
                this.rl.prompt();
            });
    }

    /**
     * Run list command
     *
     * @private
     * @return {Promise}
     */
    runList() {
        return this.watchedSeeker.list()
            .then(list => list.forEach(item => console.log(item)))
            .catch(console.error)
            .then(() => this.rl.prompt());
    }

    /**
     * Run add path command
     *
     * @private
     * @return {Promise}
     */
    runAdd(path, argv) {
        let options = this.manageFileOptions(argv);
        return this.managePromise(this.watchedSeeker.add(path, options));
    }

    /**
     * run update options command
     *
     * @private
     * @param path {string} - the path to update
     * @param argv {object} - the new options
     * @return {Promise}
     */
    runUpdate(path, argv) {
        let options = this.manageFileOptions(argv);
        return this.managePromise(this.watchedSeeker.update(path, options));
    }

    /**
     * Run remove path command
     *
     * @private
     * @param path {string} - the path to remove
     * @return {Promise}
     */
    runRemove(path) {
        return this.managePromise(this.watchedSeeker.remove(path));
    }

    /**
     * Run restor path command
     * @param path {string} - the path to restore
     * @param argv {object} - arguments command
     * @return {Promise}
     */
    runRestore(path, argv) {
        return this.managePromise(this.manager.restore(path, argv.versionized))
            .then(result => {
                delete argv.versionized;
                return result;
            });
    }

    /**
     * Retrieve cli options from commander
     *
     * @private
     * @return Object - options from CLI
     */
    manageFileOptions(argv) {
        let options = {};
        ['create', 'update', 'delete', 'recursive', 'versionized', 'keep'].forEach(option => {
            if (argv[option]) {
                options[option] = argv[option] || true;
                // FIX: it seems commander.js doesn't destroy old options,
                // so options enterred from previous commands are still read.
                delete argv[option];
            }
        });

        if (options.keep) {
            options.versionized = true;
        }

        return options;
    }

    /**
     * run commander's help command
     *
     * @private
     * @return {Promise}
     */
    runHelp() {
        return this.managePromise(
            new Promise((resolve) => {
                commander.outputHelp();
                resolve();
            })
        );
    }

    /**
     * Handle unknown Command by displaying an error
     *
     * @private
     * @param cmd {string} - the unknown command
     * @return {Promise}
     */
    unknownCommand(cmd) {
        return this.managePromise(
            new Promise((resolve, reject) => {
                if (cmd) {
                    reject(`${cmd} - Unknown command`);
                } else {
                    resolve();
                }
            })
        );
    }
}

module.exports = Cli;

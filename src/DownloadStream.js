let nodeFS = require('fs');
let path = require('path');
let fs = require('q-io/fs');
let {Duplex, PassThrough} = require('stream');
let Targz = require('tar.gz');

/**
 * DownloadStream handles file contents that will be send by server.
 * In particular, it handles tarball by decompressing it a manages each tarball file entry.
 */
class DownloadStream extends Duplex {

    constructor() {
        super();
        this.inStream = new PassThrough();
    }

    /**
     * Make the Stream starts writing the input stream into disk.
     * Manage a file or a tarball, depending on contentType,
     * and write the content into path
     *
     * @param path - the path where to write content into disk
     * @param contentType - the contentType of the input stream
     */
    start(path, contentType) {
        if (contentType === 'application/tar+gzip') {
            this.manageTarballStream(path);
        }
        else {
            this.manageFileStream(path);
        }

        return this;
    }

    _read(n) {
    }

    _write(chunk, enc, cb) {
        this.inStream.write(chunk, enc, cb)
    }

    pipe(next) {
        this.inStream.pipe(next, {end: false});
        return this;
    }

    /**
     * Write the input stream as a file into disk
     *
     * @private
     * @param filePath - the path of the file to write
     */
    manageFileStream(filePath) {
        let splittedPath = filePath.split(path.posix.sep);
        let directoryPath = splittedPath.slice(0, splittedPath.length - 1).join(path.posix.sep);

        fs.makeTree(directoryPath)
            .then(() => this.pipe(nodeFS.createWriteStream(filePath)))
            .catch(err => this.emit('error', err));
    }

    /**
     * Write the input stream as a tarball into disk
     *
     * @private
     * @param directoryPath - the path of the tarball to extract
     */
    manageTarballStream(directoryPath) {
        let parse = new Targz().createParseStream();
        parse.on('entry', entry => this.manageTarballEntry(directoryPath, entry));
        this.pipe(parse);
    }

    /**
     * Manage a tarball entry by writing the content of each file into the disk.
     * Warning: this will not create empty folders, for performance reasons.
     *
     * @private
     * @param requestedPath - the path where to extract the tarball entry
     * @param entry -the entry to extract
     * @return {Promise}
     */
    manageTarballEntry(requestedPath, entry) {
        if (entry.type === 'File') {
            let absoluteEntryPath = path.join(requestedPath, entry.path);
            let splittedPath = absoluteEntryPath.split(path.posix.sep);
            let directoryPath = splittedPath
                .slice(0, splittedPath.length - 1)
                .join(path.posix.sep);

            return fs.makeTree(directoryPath)
                .then(() => entry.pipe(nodeFS.createWriteStream(absoluteEntryPath)));
        }
    }
}

module.exports = DownloadStream;

let path = require('path');

class Utils {
    /**
     * Convert a path, written using system encoding, into Unix encoding
     * @param thePath {string} the path to convert
     * @return {string}
     */
    static systemToPosix(thePath) {
        let systemSep = RegExp.escape(path.sep);
        let posixPath = thePath.replace(new RegExp(systemSep + '+', 'g'), path.posix.sep);
        return path.posix.normalize(posixPath);
    }
}

module.exports = Utils;
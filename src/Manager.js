'use strict';
const winston = require('winston');

let FileRequester = require('./Requesters/FileRequester');
let DownloadStream = require('./DownloadStream');
let AddedSeeker = require('./Seekers/AddedSeeker');
let RemovedSeeker = require('./Seekers/RemovedSeeker');
let WatchedSeeker = require('./Seekers/WatchedSeeker');

/**
 * Manager handles files changes.
 * Manager only sends a request if it necessary, according to:
 *  * the changed path has to be added/updated/removed from the server,
 *  * the fact the change is a file creation / updates,
 *  * the fact that the change has not been already done.
 *
 *  It also asks for seekers to stores changes information, so each change is notified
 */
class Manager {
    constructor(watchedSeeker = new WatchedSeeker(),
                addedSeeker = new AddedSeeker(),
                removedSeeker = new RemovedSeeker(),
                fileRequester = new FileRequester()) {
        this.watchedSeeker = watchedSeeker;
        this.addedSeeker = addedSeeker;
        this.removedSeeker = removedSeeker;
        this.fileRequester = fileRequester;
    }

    /**
     * Manage file adding.
     *
     * @param path string the path to add
     * @param stats Stats the stats of the path to add
     * @returns {Promise}
     */
    addFile(path, stats) {
        return this.addedSeeker.isNew(path)
            .then(isNew => {
                if (!isNew) {
                    winston.log('warn', `Trying to add non-new path: ${path}`);
                    return Promise.resolve();
                }

                return this.watchedSeeker.findOptions(path)
                    .then(options => {
                        if (!options.create) {
                            return Promise.resolve();
                        }

                        return this.fileRequester.addFile(path)
                            .then(() => this.addedSeeker.add(path, stats));
                    });
            })
            .catch(err => {
                if (err.code && err.code === 'GS-EXISTS') {
                    winston.log('warning', 'Client tried to add an existing path', err);
                    return this.updateFile(path, stats);
                }

                winston.log('error', 'error while adding file', err);
                return Promise.reject(err);
            });
    }

    /**
     * Manage file updating.
     *
     * @param path string the path to update
     * @param stats Stats the stats of the path to update
     * @returns {Promise}
     */
    updateFile(path, stats) {
        return this.watchedSeeker.findOptions(path)
            .then(options => {
                if (!options.update) {
                    return Promise.resolve();
                }

                let request = options.versionize ?
                    this.fileRequester.createVersion(path, options.keep) :
                    this.fileRequester.updateFile(path);

                return request
                    .then(() => this.addedSeeker.update(path, stats));

            })
            .catch(err => {
                if (err.code && err.code === 'GS-NORESULT') {
                    winston.log('warning', 'Client tried to update a new path', err);
                    return this.addFile(path, stats);
                }

                winston.log('error', 'error while updating file', err);
                return Promise.reject(err);
            });
    }

    /**
     * Manage path removing.
     *
     * @param path {string} the path to remove
     * @returns {Promise}
     */
    removePath(path) {
        return Promise.all([
            this.watchedSeeker.findOptions(path),
            this.removedSeeker.isRemoved(path)
        ])
            .then(results => {
                let [options, isRemoved] = results;

                if (!options) {
                    winston.log('error', `${path} is not watched`);
                    throw new Error(`${path} is not watched`);
                }

                if (!options.delete || isRemoved) {
                    return Promise.resolve();
                }

                return this.fileRequester.removePath(path)
                    .then(() => this.addedSeeker.remove(path));
            })
            .catch(err => {
                if (err.code && err.code === 'GS-NORESULT') {
                    winston.log('warning', 'Client tried to remove a non existing path', err);
                    return this.addedSeeker.remove(path);
                }

                winston.log('error', 'error while adding file', err.message);
                return Promise.reject(err);
            });
    }

    /**
     * Removes a folder and it content
     * @param folderPath {string} the pat of the folder to remove.
     * @return {Promise}
     */
    removeFolder(folderPath) {
        return this.removedSeeker.markAsRemoved(folderPath)
            .then(() => this.removePath(folderPath))
            .catch(err => {
                winston.log('error', "error while removing folder: ", err.message);
                return Promise.reject(err);
            });
    }

    /**
     * Restore a path from the server
     * @param path {string} the path to restore
     * @param version {Number} the version to restore
     * @return {Promise}
     */
    restore(path, version) {
        return new Promise((resolve, reject) => {
            let downloadStream = new DownloadStream();
            console.log('version', version);
            let stream = this.fileRequester.restore(path, version);

            stream.on('response', response => {
                downloadStream.start(path, response.headers['content-type']);
            })
                .pipe(downloadStream)
                .on('finish', resolve)
                .on('error', err => {
                    winston.log('error', 'error while restoring path', err.message);
                    return reject(err);
                });
        });
    }
}

module.exports = Manager;

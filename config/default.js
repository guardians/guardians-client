const winston = require('winston');

module.exports = {
    /**
     * Server Url
     */
    "server": {
        // "url": "https://localhost:8000",
        // "strictSSl": true,
        "availabilityTest": 60
    },

    /**
     * Client credential
     */
    //    "credentials": {
    //        "username": "username",
    //        "password": "password"
    //    },
    /**
     * NEDB persistence
     */
    seeker: {
        added: 'data/addedSeeker.json',
        watched: 'data/watchedSeeker.json'
    },

    /**
     * Winston logger configuration
     */
    logger: {
        transports: [
            new (winston.transports.File)({
                filename: 'logs/errors.log',
                level: 'info'
            })
        ]
    },

    /**
     * Chokidar default options,
     * used for each watched path
     * Please see https://github.com/paulmillr/chokidar
     */
    chokidar: {
        defaultOptions: {
            ignoreInitial: true,

            /*
             * You can also replace the next line by the following
             * in order you missed so changes,
             * especially in case you have a low system.
             *
             * {
             *  awaitWriteFinish: {
             *      stabilityThreshold: 2000
             *  }
             * }
             *
             */
            awaitWriteFinish: true
        }
    }
};

const winston = require('winston');
const config = require('config');

module.exports = {
    /**
     * Server Url
     */
    "server": {
        "url": "https://localhost:8000",
        "strictSSL": false,
        "availabilityTest": 5
    },

    /**
     * Client credential
     */
    "credentials": {
        "username": "Development",
        "password": "Development123"
    },

    /**
     * Chokidar default options,
     * used for each watched path
     * Please see https://github.com/paulmillr/chokidar
     */
    chokidar: {
        defaultOptions: {
            awaitWriteFinish: false
        }
    },

    /**
     * Winston logger configuration
     */
    logger: {
        transports: [
            new (winston.transports.File)({
                filename: 'logs/errors.log',
                level: 'info'
            }),
            new (winston.transports.Console)(),
        ]
    }
};


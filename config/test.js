const winston = require('winston');
module.exports = {
    /**
     * Server Url
     */
    "server": {
        "url": "https://localhost:8080",
        "strictSSL": false,
        "availabilityTest": 0
    },
    /**
     * Client credential
     */
    "credentials": {
        "username": "test",
        "password": "test"
    },
    /**
     * Chokidar default options,
     * used for each watched path
     * Please see https://github.com/paulmillr/chokidar
     */
    chokidar: {
        defaultOptions: {
            awaitWriteFinish: false
        }
    },
    /**
     * Winston logger configuration
     */
    logger: {
        transports: []
    }
};


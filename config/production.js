const fs = require('fs');
const winston = require('winston');

let generatedConfig = JSON.parse(fs.readFileSync('./config/production.json'));
module.exports = Object.assign(generatedConfig, {
    /**
     * Server Url
     */
    "server": {
        "availabilityTest": 600
    },

    /**
     * NEDB persistence
     */
    seeker: {
        added: 'data/addedSeeker.json',
        watched: 'data/watchedSeeker.json'
    },

    /**
     * Winston logger configuration
     */
    logger: {
        transports: [
            new (winston.transports.File)({
                filename: 'logs/errors.log',
                level: 'error'
            })
        ]
    },

    /**
     * Chokidar default options,
     * used for each watched path
     * Please see https://github.com/paulmillr/chokidar
     */
    chokidar: {
        defaultOptions: {
            ignoreInitial: true,
            awaitWriteFinish: true
        }
    }
});

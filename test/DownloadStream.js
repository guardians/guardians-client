const sinon = require('sinon');
const sinonHelpers = require('sinon-helpers');
const rewire = require('rewire');
const assert = require('chai').assert;
const MemoryStream = require('memorystream');
const Targz = require('tar.gz');
const DownloadStream = rewire('../src/DownloadStream');

function createEntry(path, type) {
    let entry = new MemoryStream();
    entry.path = path;
    entry.type = type;
    return entry;
}

describe('DownloadStream', () => {
    let fsMock,
        nodeFSMock,
        targzMock,
        targzStream,
        downloadStream;

    beforeEach(() => {
        fsMock = sinon.stub();
        fsMock.makeTree = sinon.stub()
            .resolves();

        nodeFSMock = sinon.stub();

        targzStream = new MemoryStream();
        targzMock = sinonHelpers.getStubConstructor(Targz.prototype);
        targzMock.withMethods('createParseStream', sinonHelpers.returning(targzStream));

        DownloadStream.__set__({
            'fs': fsMock,
            'nodeFS': nodeFSMock,
            'Targz': targzMock
        });

        downloadStream = new DownloadStream();
    });

    it('is able to restore a file', done => {
        let inputStream = new MemoryStream(['This', 'is', 'a', 'Stream'],);
        let outputStream = new MemoryStream();
        nodeFSMock.createWriteStream = sinon.stub()
            .returns(outputStream);

        inputStream.pipe(downloadStream);
        downloadStream
            .on('finish', () => {
                setTimeout(() => {
                    assert.isTrue(fsMock.makeTree.calledOnce);
                    assert.isTrue(fsMock.makeTree.calledWith('path/to'));
                    assert.isTrue(nodeFSMock.createWriteStream.calledOnce);
                    assert.isTrue(nodeFSMock.createWriteStream.calledWith('path/to/restore'));
                    done();
                });
            })
            .start('path/to/restore', 'application/json');

        inputStream.end();
    });

    it('is able to restore a tarball', done => {
        let inputStream = new MemoryStream(['This', 'is', 'a', 'Stream'],);
        let outputStream = new MemoryStream();
        nodeFSMock.createWriteStream = sinon.stub()
            .returns(outputStream);

        inputStream.pipe(downloadStream);
        downloadStream
            .on('finish', () => {
                setTimeout(() => {
                    assert.isTrue(fsMock.makeTree.called);
                    assert.isTrue(fsMock.makeTree.calledWith('path/to/restore/tmp/folder'));
                    assert.isTrue(nodeFSMock.createWriteStream.calledWith('path/to/restore/tmp/folder/file1'));
                    assert.isTrue(nodeFSMock.createWriteStream.calledWith('path/to/restore/tmp/folder/file2'));
                    done();
                });
            })
            .start('path/to/restore', 'application/tar+gzip');

        targzStream.emit('entry', createEntry('/tmp/folder', 'Folder'));
        targzStream.emit('entry', createEntry('/tmp/folder/file1', 'File'));
        targzStream.emit('entry', createEntry('/tmp/folder/file2', 'File'));
        inputStream.end();
    });
});
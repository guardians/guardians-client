'use strict';

const sinon = require('sinon');
const rewire = require('rewire');
const assert = require('chai').assert;

const UserRequester = rewire('../../src/Requesters/UserRequester.js');

describe('UserRequester', () => {
    let requestPromiseMock,
        userRequester;

    beforeEach(() => {
        requestPromiseMock = sinon.stub();

        UserRequester.__set__({
            'requestPromise': requestPromiseMock
        });

        userRequester = new UserRequester();
    });

    describe('#add()', () => {
        it('sends add request', () => {
            let expectedParams = {
                uri: "https://localhost:8080/users",
                method: 'POST',
                body: {
                    username: 'FakeUsername',
                    password: 'FakePassword'
                },
                headers: {
                    username: 'test',
                    password: 'test'
                },

                json: true,
                strictSSL: false,
            };

            requestPromiseMock.resolves("Pretend to be the request result");

            return userRequester.add('FakeUsername', 'FakePassword')
                .then(() => {
                    assert.isTrue(requestPromiseMock.calledOnce);
                    assert.deepEqual(requestPromiseMock.getCall(0).args[0], expectedParams);
                });
        });
    });
});

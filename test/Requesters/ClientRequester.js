'use strict';

const sinon = require('sinon');
const rewire = require('rewire');
const assert = require('chai').assert;

const ClientRequester = rewire('../../src/Requesters/ClientRequester.js');

describe('ClientRequester', () => {
    let requestPromiseMock,
        clientRequester;

    beforeEach(() => {
        requestPromiseMock = sinon.stub();

        ClientRequester.__set__({
            'requestPromise': requestPromiseMock
        });

        clientRequester = new ClientRequester();
    });

    describe('#add()', () => {
        it('sends add request', () => {
            let user = {
                username: 'test',
                password: 'test'
            };
            let expectedParams = {
                uri: "https://localhost:8080/clients",
                method: 'POST',
                body: {
                    username: 'FakeUsername',
                    password: 'FakePassword'
                },
                headers: user,

                json: true,
                strictSSL: false,
            };

            requestPromiseMock.resolves("Pretend to be the request result");

            return clientRequester.add('FakeUsername', 'FakePassword', user)
                .then(() => {
                    assert.isTrue(requestPromiseMock.calledOnce);
                    assert.deepEqual(requestPromiseMock.getCall(0).args[0], expectedParams);
                });
        });
    });
});

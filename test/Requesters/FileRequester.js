'use strict';

const sinon = require('sinon');
const rewire = require('rewire');
const assert = require('chai').assert;

const FileRequester = rewire('../../src/Requesters/FileRequester.js');

describe('FileRequester', () => {
    let UtilsMock,
        fileRequester,
        requestMock,
        requestPromiseMock,
        fsMock;

    beforeEach(() => {
        UtilsMock = sinon.stub();
        requestMock = sinon.stub();
        requestPromiseMock = sinon.stub();
        fsMock = sinon.stub();

        FileRequester.__set__({
            'request': requestMock,
            'requestPromise': requestPromiseMock,
            'fs': fsMock,
            'Utils': UtilsMock
        });

        fileRequester = new FileRequester(null);
    });

    describe('#addFile()', () => {
        it('sends add request', () => {
            let expectedParams = {
                uri: "https://localhost:8080/paths",
                method: 'PUT',
                formData: {
                    path: 'system_path/to/add',
                    file: "Pretend to be a stream"
                },
                headers: {
                    username: 'test',
                    password: 'test'
                },

                json: true,
                strictSSL: false,
            };

            fsMock.createReadStream = sinon.stub()
                .returns(expectedParams.formData.file);
            UtilsMock.systemToPosix = sinon.stub()
                .returns('system_path/to/add');
            requestPromiseMock.resolves("Pretend to be the request result");

            return fileRequester.addFile('path/to/add')
                .then(() => {
                    assert.isTrue(fsMock.createReadStream.calledOnce);
                    assert.isTrue(fsMock.createReadStream.calledWith('path/to/add'));
                    assert.deepEqual(UtilsMock.systemToPosix.getCall(0).args, ['path/to/add']);
                    assert.isTrue(requestPromiseMock.calledOnce);
                    assert.deepEqual(requestPromiseMock.getCall(0).args[0], expectedParams);
                });
        });

        it('handles request rejection', () => {
            fsMock.createReadStream = sinon.stub()
                .returns("Pretend to be a stream");

            UtilsMock.systemToPosix = sinon.stub()
                .returns('system_path/to/add');

            requestPromiseMock.returns(Promise.reject({error: "Pretend to be a request error"}));

            return fileRequester.addFile('path/to/add')
                .then(() => assert.fail())
                .catch(err => {
                    assert.isTrue(fsMock.createReadStream.calledOnce);
                    assert.isTrue(fsMock.createReadStream.calledWith('path/to/add'));
                    assert.isTrue(requestPromiseMock.calledOnce);
                    assert.equal(err, "Pretend to be a request error");
                });
        });
    });

    describe('#updateFile()', () => {
        it('sends update request', () => {
            let expectedParams = {
                uri: "https://localhost:8080/paths",
                method: 'POST',
                formData: {
                    path: 'system_path/to/update',
                    file: "Pretend to be a stream"
                },
                headers: {
                    username: 'test',
                    password: 'test'
                },

                json: true,
                strictSSL: false
            };

            fsMock.createReadStream = sinon.stub()
                .returns(expectedParams.formData.file);

            UtilsMock.systemToPosix = sinon.stub()
                .returns('system_path/to/update');

            requestPromiseMock.resolves("Pretend to be the request result");

            return fileRequester.updateFile('path/to/update')
                .then(() => {
                    assert.isTrue(fsMock.createReadStream.calledOnce);
                    assert.isTrue(fsMock.createReadStream.calledWith('path/to/update'));
                    assert.isTrue(UtilsMock.systemToPosix.calledOnce);
                    assert.deepEqual(UtilsMock.systemToPosix.getCall(0).args, ['path/to/update']);
                    assert.isTrue(requestPromiseMock.calledOnce);
                    assert.deepEqual(requestPromiseMock.getCall(0).args[0], expectedParams);
                });
        });

        it('handles request rejection', () => {
            fsMock.createReadStream = sinon.stub()
                .returns("Pretend to be a stream");

            requestPromiseMock.returns(Promise.reject({error: "Pretend to be a request error"}));

            UtilsMock.systemToPosix = sinon.stub()
                .returns('system_path/to/update');

            return fileRequester.updateFile('path/to/update')
                .then(() => assert.fail())
                .catch(err => {
                    assert.isTrue(fsMock.createReadStream.calledOnce);
                    assert.isTrue(UtilsMock.systemToPosix.calledOnce);
                    assert.deepEqual(UtilsMock.systemToPosix.getCall(0).args, ['path/to/update']);
                    assert.isTrue(requestPromiseMock.calledOnce);
                    assert.equal(err, "Pretend to be a request error");
                });
        });
    });

    describe('#createVersion()', () => {
        it('sends versionize request', () => {
            let expectedParams = {
                uri: "https://localhost:8080/paths/versionize",
                method: 'POST',
                formData: {
                    path: 'system_path/to/update',
                    file: "Pretend to be a stream"
                },
                headers: {
                    username: 'test',
                    password: 'test'
                },

                json: true,
                strictSSL: false
            };

            fsMock.createReadStream = sinon.stub()
                .returns(expectedParams.formData.file);

            UtilsMock.systemToPosix = sinon.stub()
                .returns('system_path/to/update');

            requestPromiseMock.resolves("Pretend to be the request result");

            return fileRequester.createVersion('path/to/update')
                .then(() => {
                    assert.isTrue(fsMock.createReadStream.calledOnce);
                    assert.isTrue(fsMock.createReadStream.calledWith('path/to/update'));
                    assert.isTrue(UtilsMock.systemToPosix.calledOnce);
                    assert.deepEqual(UtilsMock.systemToPosix.getCall(0).args, ['path/to/update']);
                    assert.isTrue(requestPromiseMock.calledOnce);
                    assert.deepEqual(requestPromiseMock.getCall(0).args[0], expectedParams);
                });
        });

        it('keeps only {N} versions', () => {
            let expectedParams = {
                uri: "https://localhost:8080/paths/versionize",
                method: 'POST',
                body: {
                    keep: 2
                },
                formData: {
                    path: 'system_path/to/update',
                    file: "Pretend to be a stream"
                },
                headers: {
                    username: 'test',
                    password: 'test'
                },

                json: true,
                strictSSL: false
            };

            fsMock.createReadStream = sinon.stub()
                .returns(expectedParams.formData.file);

            UtilsMock.systemToPosix = sinon.stub()
                .returns('system_path/to/update');

            requestPromiseMock.resolves("Pretend to be the request result");

            return fileRequester.createVersion('path/to/update', 2)
                .then(() => {
                    assert.isTrue(fsMock.createReadStream.calledOnce);
                    assert.isTrue(fsMock.createReadStream.calledWith('path/to/update'));
                    assert.isTrue(UtilsMock.systemToPosix.calledOnce);
                    assert.deepEqual(UtilsMock.systemToPosix.getCall(0).args, ['path/to/update']);
                    assert.isTrue(requestPromiseMock.calledOnce);
                    assert.deepEqual(requestPromiseMock.getCall(0).args[0], expectedParams);
                });
        });

        it('handles request rejection', () => {
            fsMock.createReadStream = sinon.stub()
                .returns("Pretend to be a stream");

            requestPromiseMock.returns(Promise.reject({error: "Pretend to be a request error"}));

            UtilsMock.systemToPosix = sinon.stub()
                .returns('system_path/to/update');

            return fileRequester.updateFile('path/to/update')
                .then(() => assert.fail())
                .catch(err => {
                    assert.isTrue(fsMock.createReadStream.calledOnce);
                    assert.isTrue(UtilsMock.systemToPosix.calledOnce);
                    assert.deepEqual(UtilsMock.systemToPosix.getCall(0).args, ['path/to/update']);
                    assert.isTrue(requestPromiseMock.calledOnce);
                    assert.equal(err, "Pretend to be a request error");
                });
        });
    });

    describe('#removePath()', () => {
        it('sends remove request', () => {
            let expectedParams = {
                uri: "https://localhost:8080/paths",
                method: 'DELETE',
                body: {
                    path: 'system_path/to/remove'
                },
                headers: {
                    username: 'test',
                    password: 'test'
                },

                json: true,
                strictSSL: false
            };

            requestPromiseMock.resolves("Pretend to be the request result");

            UtilsMock.systemToPosix = sinon.stub()
                .returns('system_path/to/remove');

            return fileRequester.removePath('path/to/remove')
                .then(() => {
                    assert.isTrue(requestPromiseMock.calledOnce);
                    assert.isTrue(UtilsMock.systemToPosix.calledOnce);
                    assert.deepEqual(UtilsMock.systemToPosix.getCall(0).args, ['path/to/remove']);
                    assert.deepEqual(requestPromiseMock.getCall(0).args[0], expectedParams);
                });
        });

        it('handles request rejection', () => {
            requestPromiseMock.returns(Promise.reject({error: "Pretend to be a request error"}));

            UtilsMock.systemToPosix = sinon.stub()
                .returns('system_path/to/update');

            return fileRequester.removePath('path/to/remove')
                .then(() => assert.fail())
                .catch(err => {
                    assert.isTrue(requestPromiseMock.calledOnce);
                    assert.isTrue(UtilsMock.systemToPosix.calledOnce);
                    assert.deepEqual(UtilsMock.systemToPosix.getCall(0).args, ['path/to/remove']);
                    assert.equal(err, "Pretend to be a request error");
                });
        });
    });

    describe('#restore()', () => {
        it('sends restore request for latest version', () => {
            let expectedParams = {
                uri: "https://localhost:8080/paths/download/system_path%2Fto%2Frestore",
                method: 'GET',
                headers: {
                    username: 'test',
                    password: 'test'
                },
                resolveWithFullResponse: true,
                json: false,
                strictSSL: false
            };

            requestMock.resolves("Pretend to be the request result");

            UtilsMock.systemToPosix = sinon.stub()
                .returns('system_path/to/restore');

            return fileRequester.restore('path/to/restore')
                .then(() => {
                    assert.isTrue(requestMock.calledOnce);
                    assert.isTrue(UtilsMock.systemToPosix.calledOnce);
                    assert.deepEqual(UtilsMock.systemToPosix.getCall(0).args, ['path/to/restore']);
                    assert.deepEqual(requestMock.getCall(0).args[0], expectedParams);
                });
        });

        it('sends restore request for a specific version of a path', () => {
            let expectedParams = {
                uri: "https://localhost:8080/paths/download/version/2/system_path%2Fto%2Frestore",
                method: 'GET',
                headers: {
                    username: 'test',
                    password: 'test'
                },
                resolveWithFullResponse: true,
                json: false,
                strictSSL: false
            };

            requestMock.resolves("Pretend to be the request result");

            UtilsMock.systemToPosix = sinon.stub()
                .returns('system_path/to/restore');

            return fileRequester.restore('path/to/restore', 2)
                .then(() => {
                    assert.isTrue(requestMock.calledOnce);
                    assert.isTrue(UtilsMock.systemToPosix.calledOnce);
                    assert.deepEqual(UtilsMock.systemToPosix.getCall(0).args, ['path/to/restore']);
                    assert.deepEqual(requestMock.getCall(0).args[0], expectedParams);
                });
        });
    });
});

'use strict';

const sinon = require('sinon');
const rewire = require('rewire');
const assert = require('chai').assert;

const AvailabilityRequester = rewire('../../src/Requesters/AvailabilityRequester.js');

describe('AvailabilityRequester', () => {
    let requestPromiseMock,
        availabilityRequester;

    describe('#isServerAvailable()', () => {
        beforeEach(() => {
            requestPromiseMock = sinon.stub();

            AvailabilityRequester.__set__({
                'requestPromise': requestPromiseMock
            });

            availabilityRequester = new AvailabilityRequester();
        });

        it('checks the server is available', () => {
            let expectedParams = {
                uri: "https://localhost:8080/availability",
                method: 'GET',
                headers: {
                    username: 'test',
                    password: 'test'
                },
                strictSSL: false
            };

            requestPromiseMock.resolves("200 - OK");

            return availabilityRequester.isServerAvailable()
                .then(isAvailable => {
                    assert.isTrue(requestPromiseMock.calledOnce);
                    assert.deepEqual(requestPromiseMock.getCall(0).args[0], expectedParams);
                    assert.isTrue(isAvailable);
                });
        });

        it('checks the server is not available', () => {
            let expectedParams = {
                uri: "https://localhost:8080/availability",
                method: 'GET',
                headers: {
                    username: 'test',
                    password: 'test'
                },
                strictSSL: false
            };

            requestPromiseMock.rejects("An error occured");

            return availabilityRequester.isServerAvailable()
                .then(isAvailable => {
                    assert.isTrue(requestPromiseMock.calledOnce);
                    assert.deepEqual(requestPromiseMock.getCall(0).args[0], expectedParams);
                    assert.isFalse(isAvailable);
                });
        });
    });
});

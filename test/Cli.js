'use strict';

const assert = require('chai').assert;
const sinon = require('sinon');

const Cli = require('../src/Cli.js');
const WatchedSeeker = require('../src/Seekers/WatchedSeeker');
const Manager = require('../src/Manager');

describe('Cli', () => {
    let cli,
        watchedSeeker,
        manager;

    before(() => {
        watchedSeeker = sinon.createStubInstance(WatchedSeeker);
        manager = sinon.createStubInstance(Manager);
        cli = new Cli(watchedSeeker, manager);
    });

    describe('#list', () => {
        before(() => {
            watchedSeeker.list.returns(Promise.resolve([]));
        });

        it('calls WatcherSeeker#list', () => {
            cli.runCommand('list');
            assert.isTrue(watchedSeeker.list.calledOnce);
            assert.isEmpty(watchedSeeker.list.getCall(0).args);
        });
    });

    describe('#add', () => {
        before(() => {
            watchedSeeker.add.resolves();
        });

        it('calls WatcherSeeker#add with path', () => {
            cli.runCommand('add /path');
            assert.isTrue(watchedSeeker.add.calledWith('/path'));
        });

        it('calls WatcherSeeker#add with arguments', () => {
            cli.runCommand('add /path --create');
            assert.isTrue(watchedSeeker.add.calledWith('/path', {
                create: true
            }));
        });
    });

    describe('#update', () => {
        before(() => {
            watchedSeeker.update.resolves();
        });

        it('calls WatcherSeeker#update with path', () => {
            cli.runCommand('update /path');
            assert.isTrue(watchedSeeker.update.calledWith('/path'));
        });

        it('calls WatcherSeeker#update with arguments', () => {
            cli.runCommand('update /path --create');
            assert.isTrue(watchedSeeker.update.calledWith('/path', {
                create: true
            }));
        });
    });

    describe('#rm', () => {
        before(() => {
            watchedSeeker.remove.resolves();
        });

        it('calls WatcherSeeker#remove with path', () => {
            cli.runCommand('rm /path');
            assert.isTrue(watchedSeeker.remove.calledWith('/path'));
        });
    });

    describe('#restore', () => {
        before(() => {
            manager.restore.resolves();
        });

        it('calls WatcherSeeker#restore with path', () => {
            cli.runCommand('restore /path');
            assert.isTrue(manager.restore.calledWith('/path'));
        });

        it('calls WatcherSeeker#restore with path & version', () => {
            cli.runCommand('restore /path -v 2');
            assert.isTrue(manager.restore.calledWith('/path', 2));
        });
    });

    describe('#manageFileOptions', () => {
        it('manages crud flag', () => {
            let actual = cli.manageFileOptions({create: true, update: true, delete: true, unknown: 'unknown'});

            assert.deepEqual(actual, {
                create: true,
                update: true,
                delete: true
            });
            assert.isUndefined(actual.unknown);
        });

        it('manages version & keep arguments', () => {
            let actual = cli.manageFileOptions({versionized: true, keep: 5, unknown: 'unknown'});

            assert.deepEqual(actual, {
                versionized: true,
                keep: 5
            });
            assert.isUndefined(actual.unknown);
        });

        it('automatically adds versionized flag if keep is provided', () => {
            let actual = cli.manageFileOptions({keep: 5, unknown: 'unknown'});

            assert.deepEqual(actual, {
                versionized: true,
                keep: 5
            });
            assert.isUndefined(actual.unknown);
        });
    });
});

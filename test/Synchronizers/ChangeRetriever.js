'use strict';

const assert = require('chai').assert;
const sinon = require('sinon');
const rewire = require('rewire');

const ChangeRetriever = rewire('../../src/Synchronizers/ChangeRetriever');

let createStatMockResult = function (isDirectory, date) {
    return {
        isDirectory: function () {
            return isDirectory;
        },
        lastModified: function () {
            return date;
        }
    };
};

describe('ChangeRetriever', () => {
    let changeRetriever,
        fsMock,
        watchedSeekerMock,
        addedSeekerMock,
        managerMock;

    describe('#findChanged()', () => {
        beforeEach(() => {
            fsMock = sinon.stub();
            watchedSeekerMock = sinon.stub();
            addedSeekerMock = sinon.stub();
            managerMock = sinon.stub;

            ChangeRetriever.__set__({
                'fs': fsMock
            });

            changeRetriever = new ChangeRetriever(watchedSeekerMock, addedSeekerMock, managerMock);
        });

        it('is able to detect file adding', () => {
            watchedSeekerMock.all = sinon.stub()
                .resolves({
                    'file/to/watch': {create: true}
                });
            fsMock.stat = sinon.stub()
                .resolves(createStatMockResult(false, new Date(1970, 0, 1)));
            addedSeekerMock.findOptions = sinon.stub()
                .resolves(null);
            managerMock.addFile = sinon.stub()
                .resolves();

            return changeRetriever.findChanged()
                .then(() => {
                    assert.isTrue(watchedSeekerMock.all.calledOnce);
                    assert.isTrue(addedSeekerMock.findOptions.calledWith('file/to/watch'));
                    assert.isTrue(fsMock.stat.calledOnce);
                    assert.isTrue(fsMock.stat.calledWith('file/to/watch'));
                    assert.isTrue(managerMock.addFile.calledOnce);
                    assert.isTrue(managerMock.addFile.calledWith('file/to/watch'));
                });
        });

        it('doesn\'t add a file if it was already been added', () => {
            watchedSeekerMock.all = sinon.stub()
                .resolves({
                    'file/to/watch': {create: true}
                });
            fsMock.stat = sinon.stub();
            fsMock.stat.resolves(createStatMockResult(false, new Date(1970, 0, 1)));

            addedSeekerMock.findOptions = sinon.stub()
                .resolves({});
            managerMock.addFile = sinon.stub()
                .resolves();

            return changeRetriever.findChanged()
                .then(() => {
                    assert.isTrue(watchedSeekerMock.all.calledOnce);
                    assert.isTrue(addedSeekerMock.findOptions.calledWith('file/to/watch'));
                    assert.isTrue(fsMock.stat.calledOnce);
                    assert.isTrue(fsMock.stat.calledWith('file/to/watch'));
                    assert.isTrue(managerMock.addFile.notCalled);
                });
        });

        it('is able to detect folder adding', () => {
            watchedSeekerMock.all = sinon.stub()
                .resolves({
                    'folder/to/watch': {create: true}
                });

            fsMock.stat = sinon.stub()
                .withArgs('folder/to/watch')
                .resolves(createStatMockResult(true, new Date(1970, 0, 1)));
            fsMock.stat
                .resolves(createStatMockResult(false, new Date(1970, 0, 1)));
            fsMock.list = sinon.stub()
                .resolves(['first', 'second', 'third']);

            addedSeekerMock.findOptions = sinon.stub()
                .resolves(null);
            managerMock.addFile = sinon.stub()
                .resolves();

            return changeRetriever.findChanged()
                .then(() => {
                    setTimeout(() => {
                        assert.isTrue(watchedSeekerMock.all.calledOnce);
                        assert.isTrue(addedSeekerMock.findOptions.calledWith('folder/to/watch'));
                        assert.isTrue(fsMock.stat.calledOnce);
                        assert.isTrue(fsMock.stat.calledWith('folder/to/watch'));
                        assert.isTrue(managerMock.addFile.calledWith('folder/to/watch/first'));
                        assert.isTrue(managerMock.addFile.calledWith('folder/to/watch/second'));
                        assert.isTrue(managerMock.addFile.calledWith('folder/to/watch/third'));
                    });
                });
        });

        it('doesn\'t add a file if it was already been added', () => {
            watchedSeekerMock.all = sinon.stub()
                .resolves({
                    'file/to/watch': {create: true}
                });
            fsMock.stat = sinon.stub()
                .resolves(createStatMockResult(false, new Date(1970, 0, 1)));
            addedSeekerMock.findOptions = sinon.stub()
                .resolves({});
            managerMock.addFile = sinon.stub()
                .resolves();

            return changeRetriever.findChanged()
                .then(() => {
                    assert.isTrue(watchedSeekerMock.all.calledOnce);
                    assert.isTrue(addedSeekerMock.findOptions.calledWith('file/to/watch'));
                    assert.isTrue(fsMock.stat.calledOnce);
                    assert.isTrue(fsMock.stat.calledWith('file/to/watch'));
                    assert.isTrue(managerMock.addFile.notCalled);
                });
        });

        it('is able to detect file updating', () => {
            watchedSeekerMock.all = sinon.stub()
                .resolves({
                    'file/to/watch': {create: true}
                });
            fsMock.stat = sinon.stub()
                .resolves(createStatMockResult(false, new Date(2050, 0, 1)));
            addedSeekerMock.findOptions = sinon.stub()
                .resolves({
                    mtime: new Date(1970, 0, 1)
                });
            managerMock.updateFile = sinon.stub()
                .resolves();

            return changeRetriever.findChanged()
                .then(() => {
                    assert.isTrue(watchedSeekerMock.all.calledOnce);
                    assert.isTrue(addedSeekerMock.findOptions.calledWith('file/to/watch'));
                    assert.isTrue(fsMock.stat.calledOnce);
                    assert.isTrue(fsMock.stat.calledWith('file/to/watch'));
                    assert.isTrue(managerMock.updateFile.calledOnce);
                    assert.isTrue(managerMock.updateFile.calledWith('file/to/watch'));
                });
        });

        it('doesn\'t update a file which doesn\'t change', done => {
            watchedSeekerMock.all = sinon.stub()
                .resolves({
                    'file/to/watch': {create: true}
                });
            fsMock.stat = sinon.stub();
            fsMock.stat.withArgs('file/to/watch')
                .resolves(createStatMockResult(false, new Date(1970, 0, 1)));

            addedSeekerMock.findOptions = sinon.stub()
                .resolves({
                    mtime: new Date(1970, 0, 1)
                });
            managerMock.updateFile = sinon.stub()
                .resolves();

            changeRetriever.findChanged()
                .then(() => {
                    assert.isTrue(watchedSeekerMock.all.calledOnce);
                    assert.isTrue(addedSeekerMock.findOptions.calledWith('file/to/watch'));
                    assert.isTrue(fsMock.stat.calledOnce);
                    assert.isTrue(fsMock.stat.calledWith('file/to/watch'));
                    assert.isTrue(managerMock.updateFile.notCalled);
                    done();
                });
        });
    });

    describe("findRemoved()", () => {
        beforeEach(() => {
            fsMock = sinon.stub();
            watchedSeekerMock = sinon.stub();
            addedSeekerMock = sinon.stub();
            managerMock = sinon.stub;

            ChangeRetriever.__set__({
                'fs': fsMock
            });

            changeRetriever = new ChangeRetriever(watchedSeekerMock, addedSeekerMock, managerMock);
        });

        it('is able to detect file removing', done => {
            addedSeekerMock.allHierarchically = sinon.stub()
                .resolves({
                    'tmp': {
                        'removedFile': {
                            __options: {
                                isDirectory: false
                            }
                        }
                    }
                });

            fsMock.isDirectory = sinon.stub();
            fsMock.isDirectory.withArgs('/tmp')
                .resolves(true);
            fsMock.isDirectory.resolves(false);

            fsMock.exists = sinon.stub();
            fsMock.exists.withArgs('/tmp')
                .returns(true);
            fsMock.exists.returns(false);

            managerMock.removePath = sinon.stub()
                .resolves();
            managerMock.removeFolder = sinon.stub()
                .resolves();

            changeRetriever.findRemoved()
                .then(() => {
                    setTimeout(() => {
                        assert.isTrue(addedSeekerMock.allHierarchically.calledOnce);
                        assert.isTrue(fsMock.exists.calledWith('/tmp'));
                        assert.isTrue(fsMock.exists.calledWith('/tmp/removedFile'));
                        assert.isTrue(fsMock.isDirectory.calledWith('/tmp'));
                        assert.isTrue(fsMock.isDirectory.calledWith('/tmp/removedFile'));
                        assert.isTrue(managerMock.removePath.calledOnce);
                        assert.isTrue(managerMock.removePath.calledWith('/tmp/removedFile'));
                        assert.isTrue(managerMock.removeFolder.notCalled);
                        done();
                    });
                });
        });

        it('is able to detect folder removing', done => {
            addedSeekerMock.allHierarchically = sinon.stub()
                .resolves({
                    'tmp': {
                        'removedFolder': {
                            __options: {
                                isDirectory: true
                            }
                        }
                    }
                });

            fsMock.isDirectory = sinon.stub()
                .resolves(true);

            fsMock.exists = sinon.stub();
            fsMock.exists.withArgs('/tmp')
                .returns(true);
            fsMock.exists.returns(false);

            managerMock.removePath = sinon.stub()
                .resolves();
            managerMock.removeFolder = sinon.stub()
                .resolves();

            changeRetriever.findRemoved()
                .then(() => {
                    setTimeout(() => {
                        assert.isTrue(addedSeekerMock.allHierarchically.calledOnce);
                        assert.isTrue(fsMock.exists.calledWith('/tmp'));
                        assert.isTrue(fsMock.exists.calledWith('/tmp/removedFolder'));
                        assert.isTrue(fsMock.isDirectory.calledWith('/tmp'));
                        assert.isTrue(fsMock.isDirectory.calledWith('/tmp/removedFolder'));
                        assert.isTrue(managerMock.removePath.notCalled);
                        assert.isTrue(managerMock.removeFolder.calledOnce);
                        assert.isTrue(managerMock.removeFolder.calledWith('/tmp/removedFolder'));
                        done();
                    });
                });
        });

        it('doesn\'t remove an existing path', done => {
            addedSeekerMock.allHierarchically = sinon.stub()
                .resolves({
                    'tmp': {
                        'removedFile': {
                            __options: {
                                isDirectory: false
                            }
                        }
                    }
                });

            fsMock.isDirectory = sinon.stub();
            fsMock.isDirectory.withArgs('/tmp')
                .resolves(true);
            fsMock.isDirectory.resolves(false);

            fsMock.exists = sinon.stub()
                .returns(true);

            managerMock.removePath = sinon.stub()
                .resolves();
            managerMock.removeFolder = sinon.stub()
                .resolves();

            changeRetriever.findRemoved()
                .then(() => {
                    setTimeout(() => {
                        assert.isTrue(addedSeekerMock.allHierarchically.calledOnce);
                        assert.isTrue(fsMock.exists.calledWith('/tmp'));
                        assert.isTrue(fsMock.exists.calledWith('/tmp/removedFile'));
                        assert.isTrue(fsMock.isDirectory.calledWith('/tmp'));
                        assert.isTrue(fsMock.isDirectory.calledWith('/tmp/removedFile'));
                        assert.isFalse(managerMock.removePath.called);
                        done();
                    });
                });
        });
    })
});

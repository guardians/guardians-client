'use strict';

const MemoryStream = require('memorystream');
const rewire = require('rewire');
const assert = require('chai').assert;
const sinon = require('sinon');

const Watcher = rewire('../../src/Synchronizers/ChangeWatcher');
const Manager = require('../../src/Manager');
const WatchedSeeker = require('../../src/Seekers/WatchedSeeker');
const AddedSeeker = require('../../src/Seekers/AddedSeeker');
const RemovedSeeker = require('../../src/Seekers/RemovedSeeker');

describe('ChangeWatcher', () => {
    let chokidarMock,
        manager,
        watchedSeeker,
        watcher;

    let initializeStubs = function () {
        chokidarMock = new MemoryStream();
        chokidarMock.watch = sinon.stub().returns(chokidarMock);
        manager = sinon.createStubInstance(Manager);
        watchedSeeker = sinon.createStubInstance(WatchedSeeker);

        Watcher.__set__({
            'chokidar': chokidarMock
        });

        watcher = new Watcher(watchedSeeker, manager);
    };

    describe('#onAddFileDetected', () => {
        beforeEach(initializeStubs);

        it('will emit an error if add fails', () => {
            let errorSent = false;
            watcher.on('error', () => {
                errorSent = true
            });

            manager.addFile = sinon.stub()
                .rejects('FAILED');

            watcher.onAddFileDetected('path', {})
                .then(() => {
                    assert.isTrue(errorSent);
                });
        });
    });

    describe('#onAddFileDetected', () => {
        beforeEach(initializeStubs);

        it('will emit an error if add fails', () => {
            let errorSent = false;
            watcher.on('error', () => {
                errorSent = true
            });

            manager.addFile = sinon.stub()
                .rejects('FAILED');

            watcher.onAddFileDetected('path', {})
                .then(() => {
                    assert.isTrue(errorSent);
                });
        });
    });

    describe('#onChangeFileDetected', () => {
        beforeEach(initializeStubs);

        it('will emit an error if update fails', () => {
            let errorSent = false;
            watcher.on('error', () => {
                errorSent = true
            });

            manager.updateFile = sinon.stub()
                .rejects('FAILED');

            watcher.onChangeFileDetected('path', {})
                .then(() => {
                    assert.isTrue(errorSent);
                });
        });
    });

    describe('#onUnlinkDetected', () => {
        beforeEach(initializeStubs);

        it('will emit an error if add fails', () => {
            let errorSent = false;
            watcher.on('error', () => {
                errorSent = true
            });

            manager.removePath = sinon.stub()
                .rejects('FAILED');

            watcher.onUnlinkDetected('path', {})
                .then(() => {
                    assert.isTrue(errorSent);
                });
        });
    });

    describe('#onUnlinkDirDetected', () => {
        beforeEach(initializeStubs);

        it('will emit an error if add fails', () => {
            let errorSent = false;
            watcher.on('error', () => {
                errorSent = true
            });

            manager.removeFolder = sinon.stub()
                .rejects('FAILED');

            watcher.onUnlinkDirDetected('path', {})
                .then(() => {
                    assert.isTrue(errorSent);
                });
        });
    });


    describe('#start', () => {
        beforeEach(initializeStubs);

        it("listen for file adding", () => {
            watchedSeeker.all.resolves({
                'one': {create: true},
                'two': {update: false},
                'three': {delete: true}
            });
            manager.addFile.resolves();

            return watcher.start()
                .then(() => {
                    chokidarMock.emit('add', 'one', {});
                    setTimeout(() => {
                        assert.isTrue(watchedSeeker.all.calledOnce);
                        assert.isTrue(chokidarMock.watch.calledThrice);
                        assert.isTrue(chokidarMock.watch.calledWith('one', {create: true}));
                        assert.isTrue(chokidarMock.watch.calledWith('two', {update: false}));
                        assert.isTrue(chokidarMock.watch.calledWith('three', {delete: true}));
                        assert.isTrue(manager.addFile.calledOnce);
                        assert.isTrue(manager.addFile.calledWith('one', {}));
                        assert.isFalse(manager.addFile.calledWith('two', {}));
                        assert.isFalse(manager.addFile.calledWith('three', {}));
                    });
                });
        });

        it("listen for file updating", () => {
            watchedSeeker.all.resolves({
                'one': {create: true},
                'two': {update: false},
                'three': {delete: true}
            });
            manager.updateFile.resolves();

            return watcher.start()
                .then(() => {
                    chokidarMock.emit('change', 'one', {});
                    setTimeout(() => {
                        assert.isTrue(watchedSeeker.all.calledOnce);
                        assert.isTrue(chokidarMock.watch.calledThrice);
                        assert.isTrue(chokidarMock.watch.calledWith('one', {create: true}));
                        assert.isTrue(chokidarMock.watch.calledWith('two', {update: false}));
                        assert.isTrue(chokidarMock.watch.calledWith('three', {delete: true}));
                        assert.isTrue(manager.updateFile.calledOnce);
                        assert.isTrue(manager.updateFile.calledWith('one', {}));
                        assert.isFalse(manager.updateFile.calledWith('two', {}));
                        assert.isFalse(manager.updateFile.calledWith('three', {}));
                    }, 0);
                });
        });

        it("listen for file removing", () => {
            watchedSeeker.all.resolves({
                'one': {create: true},
                'two': {update: false},
                'three': {delete: true}
            });
            manager.removePath.resolves();

            return watcher.start()
                .then(() => {
                    chokidarMock.emit('unlink', 'one', {});
                    setTimeout(() => {
                        assert.isTrue(watchedSeeker.all.calledOnce);
                        assert.isTrue(chokidarMock.watch.calledThrice);
                        assert.isTrue(chokidarMock.watch.calledWith('one', {create: true}));
                        assert.isTrue(chokidarMock.watch.calledWith('two', {update: false}));
                        assert.isTrue(chokidarMock.watch.calledWith('three', {delete: true}));
                        assert.isTrue(manager.removePath.calledOnce);
                        assert.isTrue(manager.removePath.calledWith('one', {}));
                        assert.isFalse(manager.removePath.calledWith('two', {}));
                        assert.isFalse(manager.removePath.calledWith('three', {}));
                    });
                });
        });

        it("listen for folder removing", () => {
            watchedSeeker.all = sinon.stub().resolves({'one': {}, 'two': {}, 'three': {}});
            manager.removeFolder.resolves();

            return watcher.start()
                .then(() => {
                    chokidarMock.emit('unlinkDir', 'one', {});

                    setTimeout(() => {
                        assert.isTrue(watchedSeeker.all.calledOnce);
                        assert.isTrue(chokidarMock.watch.calledThrice);
                        assert.isTrue(chokidarMock.watch.calledWith('one', {}));
                        assert.isTrue(chokidarMock.watch.calledWith('two', {}));
                        assert.isTrue(chokidarMock.watch.calledWith('three', {}));
                        assert.isTrue(manager.removeFolder.calledOnce);
                        assert.isTrue(manager.removeFolder.calledWith('one', {}));
                        assert.isFalse(manager.removeFolder.calledWith('two', {}));
                        assert.isFalse(manager.removeFolder.calledWith('three', {}));
                    });
                });
        });
    });

    describe('#stop', () => {
        beforeEach(initializeStubs);

        it("stops watching paths", () => {
            watchedSeeker.all.resolves({
                'one': {create: true}
            });

            let watchedStub = sinon.stub();
            watchedStub.unwatch = sinon.stub();
            watchedStub.close = sinon.stub();
            watchedStub.on = sinon.stub()
                .returns(watchedStub);

            chokidarMock.watch = sinon.stub()
                .returns(watchedStub);

            return watcher.start()
                .then(() => {
                    watcher.stop();
                    assert.isUndefined(watcher.watched);
                    assert.isTrue(chokidarMock.watch.calledOnce);
                    assert.isTrue(watchedStub.unwatch.calledOnce);
                    assert.isTrue(watchedStub.close.calledOnce);
                });
        });
    });
});


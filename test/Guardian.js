'use strict';

const sinon = require('sinon');
const rewire = require('rewire');
const assert = require('chai').assert;
const sinonHelpers = require('sinon-helpers');

const AddedSeeker = require('../src/Seekers/AddedSeeker');
const WatchedSeeker = require('../src/Seekers/WatchedSeeker');
const Manager = require('../src/Manager');
const ChangeWatcher = require('../src/Synchronizers/ChangeWatcher');
const ChangeRetriever = require('../src/Synchronizers/ChangeRetriever');
const AvailabilityRequester = require('../src/Requesters/AvailabilityRequester');

const Guardian = rewire('../src/Guardian');

describe('Guardian', () => {
    describe('#start()', () => {
        let addedSeekerMock,
            watchedSeekerMock,
            managerMock,
            changeWatcherMock,
            changeRetrieverMock,
            availabilityRequesterMock,
            guardian;

        beforeEach(() => {
            addedSeekerMock = sinonHelpers.getStubConstructor(AddedSeeker.prototype);
            watchedSeekerMock = sinonHelpers.getStubConstructor(WatchedSeeker.prototype);
            managerMock = sinonHelpers.getStubConstructor(Manager.prototype);
            changeWatcherMock = sinonHelpers.getStubConstructor(ChangeWatcher.prototype);
            changeRetrieverMock = sinonHelpers.getStubConstructor(ChangeRetriever.prototype);
            availabilityRequesterMock = sinonHelpers.getStubConstructor(AvailabilityRequester.prototype);

            Guardian.__set__({
                'AddedSeeker': addedSeekerMock,
                'WatchedSeeker': watchedSeekerMock,
                'Manager': managerMock,
                'ChangeWatcher': changeWatcherMock,
                'ChangeRetriever': changeRetrieverMock,
                'AvailabilityRequester': availabilityRequesterMock
            });

            guardian = new Guardian();
        });

        it("starts synchronizers", () => {
            changeWatcherMock.getInstance()
                .start = sinon.stub().resolves();
            changeWatcherMock.getInstance()
                .stop = sinon.stub().resolves();
            changeWatcherMock.getInstance()
                .once = sinon.stub();

            changeRetrieverMock.getInstance()
                .start = sinon.stub().resolves();

            return guardian.start()
                .then(() => {
                    assert.equal(changeRetrieverMock.getInstances().length, 1);
                    assert.isTrue(changeRetrieverMock.getInstance().start.calledOnce);
                    assert.equal(changeWatcherMock.getInstances().length, 1);
                    assert.isTrue(changeWatcherMock.getInstance().start.calledOnce);
                });
        });

        it("Manage server unreachable caught by ChangeRetriever", done => {
            changeWatcherMock.getInstance()
                .start = sinon.stub().resolves();
            changeWatcherMock.getInstance()
                .stop = sinon.stub().resolves();
            changeWatcherMock.getInstance()
                .once = sinon.stub();

            availabilityRequesterMock.getInstance()
                .isServerAvailable = sinon.stub().resolves(true);

            changeRetrieverMock.getInstance()
                .start = sinon.stub()
                .onCall(0).rejects({code: "ECONNREFUSED"})
                .onCall(1).resolves("OK");

            guardian.start()
                .then(() => {
                    setTimeout(() => {
                        assert.equal(changeRetrieverMock.getInstances().length, 1);
                        assert.isTrue(changeRetrieverMock.getInstance().start.calledTwice);
                        assert.equal(changeWatcherMock.getInstances().length, 1);
                        assert.isTrue(changeWatcherMock.getInstance().start.calledOnce);
                        assert.isTrue(changeWatcherMock.getInstance().stop.calledOnce);
                        assert.isTrue(availabilityRequesterMock.getInstance().isServerAvailable.calledOnce);
                        done();
                    });
                });
        });

        it("Manage login error & internal server error caught by ChangeRetriever", done => {
            changeWatcherMock.getInstance()
                .start = sinon.stub().resolves();
            changeWatcherMock.getInstance()
                .stop = sinon.stub().resolves();
            changeWatcherMock.getInstance()
                .once = sinon.stub();

            availabilityRequesterMock.getInstance()
                .isServerAvailable = sinon.stub().resolves(true);

            changeRetrieverMock.getInstance()
                .start = sinon.stub().rejects({statusCode: 500});

            guardian.start()
                .then(() => {
                    setTimeout(() => {
                        assert.equal(changeRetrieverMock.getInstances().length, 1);
                        assert.isTrue(changeRetrieverMock.getInstance().start.calledOnce);
                        assert.equal(changeWatcherMock.getInstances().length, 1);
                        assert.isTrue(changeWatcherMock.getInstance().start.notCalled);
                        assert.isTrue(changeWatcherMock.getInstance().stop.calledOnce);
                        assert.isTrue(availabilityRequesterMock.getInstance().isServerAvailable.notCalled);
                        done();
                    });
                });
        });
    });
});

const path = require('path');
const assert = require('chai').assert;
const rewire = require('rewire');
const Utils = rewire("../src/Utils");

describe('Utils for POSIX', () => {
    describe('#systemToPosix', () => {
        it('handles posix to posix paths', () => {
            let actual = Utils.systemToPosix('/tmp/test/file');
            assert.equal(actual, '/tmp/test/file');
        });
    });
});

describe('Utils for WIN32', () => {
    describe('#systemToPosix', () => {
        before(() => {
            Utils.__set__({
                'path': path.win32
            });
        });

        it('handles win32 to posix paths', () => {
            let actual = Utils.systemToPosix('\\tmp\\test\\file');
            assert.equal(actual, '/tmp/test/file');
        });

        it('normalizes paths', () => {
            let actual = Utils.systemToPosix('\\tmp\\\\test\\file');
            assert.equal(actual, '/tmp/test/file');
        });

        after(() => {
            Utils.__set__({
                'path': path
            });
        });
    });
});
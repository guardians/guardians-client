'use strict';

const sinon = require('sinon');
const rewire = require('rewire');
const assert = require('chai').assert;
const sinonHelpers = require('sinon-helpers');
const FileRequester = require('../src/Requesters/FileRequester');
const DownloadStream = require('../src/DownloadStream');
const AddedSeeker = require('../src/Seekers/AddedSeeker');
const RemovedSeeker = require('../src/Seekers/RemovedSeeker');
const WatchedSeeker = require('../src/Seekers/WatchedSeeker');
const Manager = rewire('../src/Manager.js');

describe('Manager', () => {
    let fileRequesterMock,
        manager,
        watchedSeekerMock,
        addedSeekerMock,
        removedSeekerMock;

    beforeEach(() => {
        fileRequesterMock = sinonHelpers.getStubConstructor(FileRequester.prototype);
        fileRequesterMock.addFile = sinon.stub().resolves();
        fileRequesterMock.updateFile = sinon.stub().resolves();

        watchedSeekerMock = sinon.createStubInstance(WatchedSeeker);
        addedSeekerMock = sinon.createStubInstance(AddedSeeker);
        removedSeekerMock = sinon.createStubInstance(RemovedSeeker);

        manager = new Manager(
            watchedSeekerMock,
            addedSeekerMock,
            removedSeekerMock,
            fileRequesterMock);
    });

    describe('#addFile()', () => {
        it("doesn't try to add file if create option is disabled", () => {
            watchedSeekerMock.findOptions.returns(Promise.resolve({create: false}));
            addedSeekerMock.add.returns(Promise.resolve());
            addedSeekerMock.isNew.returns(Promise.resolve(true));

            return manager.addFile("/tmp/test", {})
                .then(() => {
                    assert.isTrue(watchedSeekerMock.findOptions.calledOnce);
                    assert.isTrue(fileRequesterMock.addFile.notCalled);
                    assert.isTrue(addedSeekerMock.add.notCalled);
                }).catch(err => {
                    assert.isTrue(false, err);
                });
        });

        it("doesn't try to add file if it's already added", () => {
            watchedSeekerMock.findOptions.returns(Promise.resolve({create: true}));
            addedSeekerMock.isNew.returns(Promise.resolve(false));

            return manager.addFile("/tmp/test", {})
                .then(() => {
                    assert.isTrue(addedSeekerMock.isNew.calledOnce);
                    assert.isTrue(addedSeekerMock.isNew.calledWith('/tmp/test'));
                    assert.isTrue(fileRequesterMock.addFile.notCalled);
                    assert.isTrue(watchedSeekerMock.findOptions.notCalled);
                    assert.isTrue(addedSeekerMock.add.notCalled);
                });
        });

        it('correctly adds file', () => {
            watchedSeekerMock.findOptions.returns(Promise.resolve({create: true}));
            addedSeekerMock.isNew.returns(Promise.resolve(true));

            return manager.addFile('path/to/add')
                .then(() => {
                    assert.isTrue(fileRequesterMock.addFile.calledOnce);
                    assert.isTrue(fileRequesterMock.addFile.calledWith('path/to/add'));
                    assert.isTrue(addedSeekerMock.add.calledOnce);
                    assert.isTrue(addedSeekerMock.add.calledWith('path/to/add'));
                });
        });

        it('handles server desynchronization', () => {
            watchedSeekerMock.findOptions.returns(Promise.resolve({
                create: true,
                update: true
            }));
            addedSeekerMock.isNew.returns(Promise.resolve(true));
            fileRequesterMock.addFile = sinon.stub().rejects({
                code: 'GS-EXISTS',
                message: 'This file already exists'
            });

            return manager.addFile('path/to/add')
                .then(() => {
                    assert.isTrue(fileRequesterMock.addFile.calledOnce);
                    assert.isTrue(fileRequesterMock.addFile.calledWith('path/to/add'));
                    assert.isTrue(addedSeekerMock.add.notCalled);
                    assert.isTrue(fileRequesterMock.updateFile.calledOnce);
                    assert.isTrue(fileRequesterMock.updateFile.calledWith('path/to/add'));
                    assert.isTrue(addedSeekerMock.update.calledOnce);
                    assert.isTrue(addedSeekerMock.update.calledWith('path/to/add'));
                });
        });
    });

    describe('#updateFile()', () => {
        it('doesn\'t try to update file if update option is disabled', () => {
            watchedSeekerMock.findOptions.resolves({update: false});
            addedSeekerMock.update.returns(Promise.resolve());
            fileRequesterMock.updateFile.resolves('RESULT');

            return manager.updateFile('path/to/update')
                .then(() => {
                    assert.isTrue(watchedSeekerMock.findOptions.calledOnce);
                    assert.isTrue(fileRequesterMock.updateFile.notCalled);
                    assert.isTrue(addedSeekerMock.update.notCalled);
                });
        });

        it('updates a file', () => {
            watchedSeekerMock.findOptions.resolves({update: true});
            addedSeekerMock.update.returns(Promise.resolve());

            return manager.updateFile('path/to/update')
                .then(() => {
                    assert.isTrue(fileRequesterMock.updateFile.calledOnce);
                    assert.isTrue(fileRequesterMock.updateFile.calledWith('path/to/update'));
                });
        });

        it('creates new version of a file', () => {
            watchedSeekerMock.findOptions.resolves({
                update: true,
                versionize: true
            });
            addedSeekerMock.update.returns(Promise.resolve());

            fileRequesterMock.updateFile = sinon.stub();
            fileRequesterMock.createVersion = sinon.stub()
                .resolves();

            return manager.updateFile('path/to/update')
                .then(() => {
                    assert.isTrue(fileRequesterMock.updateFile.notCalled);
                    assert.isTrue(fileRequesterMock.createVersion.calledOnce);
                    assert.isTrue(fileRequesterMock.createVersion.calledWith('path/to/update'));
                });
        });

        it('handles server desynchronization', () => {
            watchedSeekerMock.findOptions.returns(Promise.resolve({
                create: true,
                update: true
            }));

            addedSeekerMock.add.returns(Promise.resolve());
            addedSeekerMock.isNew.returns(Promise.resolve(true));

            fileRequesterMock.updateFile = sinon.stub().rejects({
                code: 'GS-NORESULT',
                message: 'This file doesn\'t exist'
            });

            return manager.updateFile('path/to/update')
                .then(() => {
                    assert.isTrue(fileRequesterMock.updateFile.calledOnce);
                    assert.isTrue(fileRequesterMock.updateFile.calledWith('path/to/update'));
                    assert.isTrue(addedSeekerMock.update.notCalled);
                    assert.isTrue(fileRequesterMock.addFile.calledOnce);
                    assert.isTrue(fileRequesterMock.addFile.calledWith('path/to/update'));
                    assert.isTrue(addedSeekerMock.add.calledOnce);
                    assert.isTrue(addedSeekerMock.add.calledWith('path/to/update'));
                });
        });
    });

    describe('#removePath()', () => {
        it("doesn't try to remove file if delete option is disabled", () => {
            removedSeekerMock.isRemoved.resolves(false);
            watchedSeekerMock.findOptions.resolves({delete: false});

            return manager.removePath("/tmp/test")
                .then(() => {
                    assert.isTrue(watchedSeekerMock.findOptions.calledOnce);
                    assert.isTrue(fileRequesterMock.removePath.notCalled);
                    assert.isTrue(addedSeekerMock.remove.notCalled);
                });
        });

        it("doesn't try to remove a path if it's already marked as removed", () => {
            watchedSeekerMock.findOptions.returns(Promise.resolve({delete: true}));
            addedSeekerMock.remove.returns(Promise.resolve({}));

            removedSeekerMock.isRemoved.returns(Promise.resolve(true));
            removedSeekerMock.markAsRemoved.returns(Promise.resolve());

            return manager.removePath("/tmp/test")
                .then(() => {
                    assert.isTrue(watchedSeekerMock.findOptions.calledOnce);
                    assert.isTrue(removedSeekerMock.isRemoved.calledOnce);
                    assert.isTrue(addedSeekerMock.remove.notCalled);
                    assert.isTrue(removedSeekerMock.markAsRemoved.notCalled);
                });
        });

        it('Correcly removes a file', () => {
            watchedSeekerMock.findOptions.returns(Promise.resolve({delete: true}));
            addedSeekerMock.remove.resolves();
            removedSeekerMock.isRemoved.returns(Promise.resolve(false));
            fileRequesterMock.removePath.resolves();

            return manager.removePath('path/to/remove')
                .then(() => {
                    assert.isTrue(fileRequesterMock.removePath.calledOnce);
                    assert.isTrue(fileRequesterMock.removePath.calledWith('path/to/remove'));
                    assert.isTrue(addedSeekerMock.remove.calledOnce);
                    assert.isTrue(addedSeekerMock.remove.calledWith('path/to/remove'));
                });
        });

        it('handles server desynchronization', () => {
            watchedSeekerMock.findOptions.returns(Promise.resolve({delete: true}));
            addedSeekerMock.remove.returns(Promise.resolve());
            fileRequesterMock.removePath = sinon.stub().rejects({
                code: 'GS-NORESULT',
                message: 'This file doesn\'t exist'
            });

            return manager.removePath('path/to/remove')
                .then(() => {
                    assert.isTrue(fileRequesterMock.removePath.calledOnce);
                    assert.isTrue(fileRequesterMock.removePath.calledWith('path/to/remove'));
                    assert.isTrue(addedSeekerMock.remove.calledOnce);
                    assert.isTrue(addedSeekerMock.remove.calledWith('path/to/remove'));
                });
        });
    });

    describe("#removeFolder", () => {
        it("doesn't try to remove file if delete option is disabled", () => {
            removedSeekerMock.isRemoved.resolves(false);
            watchedSeekerMock.findOptions.resolves({delete: false});

            return manager.removePath("/tmp/test")
                .then(() => {
                    assert.isTrue(watchedSeekerMock.findOptions.calledOnce);
                    assert.isTrue(fileRequesterMock.removePath.notCalled);
                    assert.isTrue(addedSeekerMock.remove.notCalled);
                });
        });

        it("doesn't try to remove a path if it's already marked as removed", () => {
            watchedSeekerMock.findOptions.returns(Promise.resolve({delete: true}));
            addedSeekerMock.remove.returns(Promise.resolve({}));

            removedSeekerMock.isRemoved.returns(Promise.resolve(true));
            removedSeekerMock.markAsRemoved.returns(Promise.resolve());

            return manager.removePath("/tmp/test")
                .then(() => {
                    assert.isTrue(watchedSeekerMock.findOptions.calledOnce);
                    assert.isTrue(removedSeekerMock.isRemoved.calledOnce);
                    assert.isTrue(addedSeekerMock.remove.notCalled);
                    assert.isTrue(removedSeekerMock.markAsRemoved.notCalled);
                });
        });

        it('Correcly removes a folder', () => {
            watchedSeekerMock.findOptions.returns(Promise.resolve({delete: true}));
            addedSeekerMock.remove.resolves();
            removedSeekerMock.isRemoved.returns(Promise.resolve(false));
            removedSeekerMock.markAsRemoved.resolves();
            fileRequesterMock.removePath.resolves();

            return manager.removeFolder('path/to/remove')
                .then(() => {
                    assert.isTrue(removedSeekerMock.markAsRemoved.calledOnce);
                    assert.isTrue(removedSeekerMock.markAsRemoved.calledWith('path/to/remove'));
                    assert.isTrue(fileRequesterMock.removePath.calledOnce);
                    assert.isTrue(fileRequesterMock.removePath.calledWith('path/to/remove'));
                    assert.isTrue(addedSeekerMock.remove.calledOnce);
                    assert.isTrue(addedSeekerMock.remove.calledWith('path/to/remove'));
                });
        });

        it('handles server desynchronization', () => {
            removedSeekerMock.markAsRemoved.resolves();
            watchedSeekerMock.findOptions.returns(Promise.resolve({delete: true}));
            addedSeekerMock.remove.returns(Promise.resolve());
            fileRequesterMock.removePath = sinon.stub().rejects({
                code: 'GS-NORESULT',
                message: 'This file doesn\'t exist'
            });

            return manager.removeFolder('path/to/remove')
                .then(() => {
                    assert.isTrue(fileRequesterMock.removePath.calledOnce);
                    assert.isTrue(fileRequesterMock.removePath.calledWith('path/to/remove'));
                    assert.isTrue(addedSeekerMock.remove.calledOnce);
                    assert.isTrue(addedSeekerMock.remove.calledWith('path/to/remove'));
                });
        });
    });
});

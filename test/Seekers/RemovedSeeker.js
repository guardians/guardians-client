'use strict';

const tmp = require('tmp');
const assert = require('chai').assert;
const fs = require('q-io/fs');

const RemovedSeeker = require('../../src/Seekers/RemovedSeeker.js');

function posixToSystem(filepath) {
    return fs.join(filepath.split('/'));
}

describe('RemovedSeeker', () => {
    let removedSeeker;

    describe('#isRemoved()', () => {
        before(done => {
            removedSeeker = new RemovedSeeker();
            removedSeeker.datastore.insert([
                    {
                        _id: posixToSystem('/tmp/folder'),
                        isDirectory: false
                    }],
                () => done());
        });

        it('is able to find the exactly removed path', () => {
            return removedSeeker.isRemoved(posixToSystem('/tmp/folder'))
                .then(assert.isTrue);
        });

        it("is able to find a removed subpath", () => {
            return removedSeeker.isRemoved(posixToSystem('/tmp/folder/file'))
                .then(assert.isTrue);
        });


        it("returns false for a non removed path", () => {
            return removedSeeker.isRemoved(posixToSystem("/tmp/folderActuallyHere"))
                .then(assert.isFalse);
        })
    });

    describe('#markAsRemoved()', () => {
        beforeEach(() => {
            removedSeeker = new RemovedSeeker();
        });

        it('adds the file', done => {
            removedSeeker.markAsRemoved(posixToSystem('/tmp/file'))
                .then(() => {
                    removedSeeker.datastore.find({},
                        (err, actualConfig) => {
                            let expectedConfig = [{
                                _id: posixToSystem('/tmp/file')
                            }];

                            assert.isNull(err);
                            assert.deepEqual(actualConfig, expectedConfig);
                            done();
                        });
                });
        });
    });
});

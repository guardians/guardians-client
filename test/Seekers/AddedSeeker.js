'use strict';

const tmp = require('tmp');
const assert = require('chai').assert;
const fs = require('q-io/fs');

const AddedSeeker = require('../../src/Seekers/AddedSeeker.js');

function posixToSystem(filepath) {
    return fs.join(filepath.split('/'));
}

describe('AddedSeeker', () => {
    let addedSeeker;

    describe('#allHierarchically()', () => {
        beforeEach(done => {
            addedSeeker = new AddedSeeker(null);
            addedSeeker.datastore.insert([
                    {
                        _id: posixToSystem('/tmp/folder'),
                        isDirectory: true
                    },
                    {
                        _id: posixToSystem('/tmp/folder/file1'),
                        isDirectory: false
                    },
                    {
                        _id: posixToSystem('/tmp/folder/file2'),
                        option: "option"
                    }],
                done);
        });

        it('is able to create hierarchy', () => {
            let expected = {
                'tmp': {
                    'folder': {
                        __options: {
                            isDirectory: true
                        },
                        'file1': {
                            __options: {
                                isDirectory: false
                            }
                        },
                        'file2': {
                            __options: {
                                option: "option"
                            }
                        }
                    }
                }
            };

            return addedSeeker.allHierarchically()
                .then(hierarchy => assert.deepEqual(hierarchy, expected));
        });
    });

    describe('#isNew()', () => {
        it('is able to find new path', () => {
            return addedSeeker.isNew('doesnt/exist')
                .then(assert.isTrue);
        });

        it('is able to find existing path', () => {
            let fakeStats = {
                mtime: 123456,
                isDirectory: () => false
            };

            return addedSeeker.add('/tmp/test', fakeStats)
                .then(() => addedSeeker.isNew('/tmp/test'))
                .then(assert.isFalse);
        });
    });

    describe('#findOptions()', () => {
        before(done => {
            addedSeeker = new AddedSeeker();
            addedSeeker.datastore.insert([
                    {
                        _id: posixToSystem('/tmp'),
                        isDirectory: true
                    },
                    {
                        _id: posixToSystem('/tmp/folder'),
                        isDirectory: false
                    },
                    {
                        _id: posixToSystem('/tmp/folder/file'),
                        isDirectory: true
                    }],
                () => done());
        });

        it('is able to find a file', () => {
            let expected = {isDirectory: true};

            return addedSeeker.findOptions(posixToSystem('/tmp/folder/file'))
                .then(actual => assert.deepEqual(actual, expected));
        });

        it("returns null if the path doesn't exists", () => {
            return addedSeeker.findOptions(posixToSystem('/tmp/folder/doesntExists'))
                .then(assert.isNull);
        });
    });

    describe('#add()', () => {
        beforeEach(() => {
            addedSeeker = new AddedSeeker(null);
        });

        it('adds the file', done => {
            let fakeStats = {
                mtime: 123456,
                isDirectory: () => false
            };
            addedSeeker.add(posixToSystem('/tmp/file'), fakeStats)
                .then(() => {
                    addedSeeker.datastore.find({},
                        (err, actualConfig) => {
                            let expectedConfig = [{
                                _id: posixToSystem('/tmp/file'),
                                mtime: 123456,
                                isDirectory: false
                            }];

                            assert.isNull(err);
                            assert.sameDeepMembers(actualConfig, expectedConfig);
                            done();
                        });
                });
        });
    });

    describe('#update()', () => {
        beforeEach(() => {
            addedSeeker = new AddedSeeker(null);
        });

        it('refuses to update a path which doesn\'t exist', () => {
            let fakeAddStats = {
                mtime: 123456,
                isDirectory: () => false
            };

            let testedPath = posixToSystem('/doesnt/exist');
            return addedSeeker.update(testedPath, fakeAddStats)
                .catch(err => assert.equal(err, "Unable to update " + testedPath + " because it doesn't exist"));
        });

        it('updates the file', done => {
            let fakeAddStats = {
                mtime: 123456,
                isDirectory: () => false
            };

            let fakeUpdateStats = {
                mtime: 323496,
                isDirectory: () => false
            };

            addedSeeker.add(posixToSystem('/tmp/file'), fakeAddStats)
                .then(addedSeeker.update(posixToSystem('/tmp/file'), fakeUpdateStats))
                .then(() => {
                    addedSeeker.datastore.find({},
                        (err, actualConfig) => {
                            let expectedConfig = [{
                                _id: posixToSystem('/tmp/file'),
                                mtime: 323496,
                                isDirectory: false
                            }];

                            assert.isNull(err);
                            assert.sameDeepMembers(actualConfig, expectedConfig);
                            done();
                        });
                });
        });
    });

    describe('#remove()', () => {
        beforeEach(done => {
            addedSeeker = new AddedSeeker(null);
            addedSeeker.datastore.insert([
                    {
                        _id: posixToSystem('/tmp/test'),
                        isDirectory: true
                    },
                    {
                        _id: posixToSystem('/tmp/folder/file1'),
                        isDirectory: false
                    },
                    {
                        _id: posixToSystem('/tmp/folder/file2'),
                        isDirectory: false
                    }],
                done);
        });

        it('is able to remove a folder and its content', done => {
            addedSeeker.remove(posixToSystem('/tmp/folder'))
                .then(() => {
                    addedSeeker.datastore.find(
                        {},
                        (err, res) => {
                            let expected = [
                                {
                                    _id: posixToSystem('/tmp/test'),
                                    isDirectory: true
                                }];
                            assert.isNull(err);
                            assert.sameDeepMembers(res, expected);
                            done();
                        });
                })
                .catch((err) => {
                    assert.isOk(false, err);
                    done();
                });
        });

        it('is able to remove a file', done => {
            addedSeeker.remove(posixToSystem('/tmp/folder/file1'))
                .then(() => {
                    addedSeeker.datastore.find(
                        {},
                        (err, res) => {
                            let expected = [
                                {
                                    _id: posixToSystem('/tmp/test'),
                                    isDirectory: true
                                },
                                {
                                    _id: posixToSystem('/tmp/folder/file2'),
                                    isDirectory: false
                                }];
                            assert.isNull(err);
                            assert.sameDeepMembers(res, expected);
                            done();
                        });
                });
        });

        it('refuses to remove an unexisting path', () => {
            return addedSeeker.remove(posixToSystem('/tmp/unexisting'))
                .then(() => assert.fail('Exception not thrown'))
                .catch(error => assert.equal(error, "Unable to remove " + posixToSystem('/tmp/unexisting') + " because it doesn't exist"));
        });
    });
});

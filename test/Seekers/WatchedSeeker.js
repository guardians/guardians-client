'use strict';

const fs = require('q-io/fs');

const assert = require('chai').assert;

const WatchedSeeker = require('../../src/Seekers/WatchedSeeker.js');

function posixToSystem(filepath) {
    return fs.join(filepath.split('/'));
}

describe('WatchedSeeker', () => {
    let watchedSeeker;

    beforeEach(() => {
        watchedSeeker = new WatchedSeeker(null);
    });

    describe('#add()', () => {
        it('add the file to watched the first time', () => {
            return watchedSeeker.add('/tmp/test')
                .then(() => watchedSeeker.all())
                .then(actualConfig => {
                    let expectedConfig = {
                        '/tmp/test': {recursive: false}
                    };
                    assert.deepEqual(actualConfig, expectedConfig);
                });
        });

        it('refuse to add the watched file twice', () => {
            return watchedSeeker.add('/tmp/test')
                .then(() => assert.fail('it should be impossible to add the same file twice !'))
                .catch(() => watchedSeeker.all())
                .then(actualConfig => {
                    let expectedConfig = {
                        '/tmp/test': {recursive: false}
                    };
                    assert.deepEqual(actualConfig, expectedConfig);
                });
        });
    });

    describe('#remove()', () => {
        it('refuse to remove an non watched file', () => {
            return watchedSeeker.remove('/tmp/test')
                .then(() => assert.fail('it should be impossible to remove an unwatch file !'))
                .catch(() => watchedSeeker.all())
                .then(actualConfig => {
                    assert.isEmpty(actualConfig);
                });
        });

        it('remove the provided file', () => {
            return watchedSeeker.add('/tmp/testToRemove')
                .then(() => watchedSeeker.add('/tmp/testToSave'))
                .then(() => watchedSeeker.remove('/tmp/testToRemove'))
                .then(() => watchedSeeker.all())
                .then(actualConfig => {
                    let expectedConfig = {
                        '/tmp/testToSave': {recursive: false}
                    };
                    assert.deepEqual(actualConfig, expectedConfig);
                });
        });
    });

    describe('#update()', () => {
        it('refuse to update an non watched file', () => {
            return watchedSeeker.update('/tmp/test', {recursive: true})
                .then(() => assert.fail('it should be impossible to update an non watched file !'))
                .catch(() => watchedSeeker.all())
                .then(actualConfig => {
                    assert.isEmpty(actualConfig);
                });
        });

        it('update the provided file', () => {
            let expectedConfig = {
                '/tmp/testToSave': {recursive: false},
                '/tmp/testToUpdate': {recursive: true}
            };

            return watchedSeeker.add('/tmp/testToUpdate')
                .then(() => watchedSeeker.add('/tmp/testToSave'))
                .then(() => watchedSeeker.update('/tmp/testToUpdate', {recursive: true}))
                .then(() => watchedSeeker.all())
                .then(actualConfig => assert.deepEqual(actualConfig, expectedConfig));
        });
    });

    describe("#list()", () => {
        it("shouldn't list any file on startup", () => {
            return watchedSeeker.list()
                .then(assert.isEmpty);
        });

        it("list 1 path", () => {
            return watchedSeeker.add('/tmp/test')
                .then(() => watchedSeeker.list())
                .then(actual => assert.deepEqual(actual, ['/tmp/test']));
        });

        it("list 2 paths", () => {
            return watchedSeeker.add('/tmp/test')
                .then(() => watchedSeeker.add('/tmp/test2'))
                .then(() => watchedSeeker.list())
                .then(actual => assert.deepEqual(actual, ['/tmp/test', '/tmp/test2']));
        });
    });

    describe("#findOptions()", () => {
        beforeEach(done => {
            watchedSeeker.datastore.insert([
                    {
                        _id: posixToSystem('/tmp/test'),
                        isDirectory: true
                    },
                    {
                        _id: posixToSystem('/tmp/test1'),
                        recursive: false,
                        create: true
                    },
                    {
                        _id: posixToSystem('/tmp/test1/sub'),
                        recursive: true,
                        update: true
                    }],
                done);
        });

        it("returns empty options if the path is not watched", () => {
            watchedSeeker.findOptions('/notWatched')
                .then(assert.isEmpty);
        });

        it("returns options for the exactly watched path", () => {
            let expected = {
                recursive: false,
                create: true
            };

            return watchedSeeker.findOptions(posixToSystem('/tmp/test1'))
                .then(actual => assert.deepEqual(actual, expected));
        });

        it("returns options for a sub watched path", () => {
            let expected = {
                recursive: true,
                create: true,
                update: true
            };

            return watchedSeeker.findOptions(posixToSystem('/tmp/test1/sub'))
                .then(actual => assert.deepEqual(actual, expected));
        });

        it("recursively add folders options", () => {
            let expected = {
                recursive: true,
                create: true,
                update: true
            };

            return watchedSeeker.findOptions(posixToSystem('/tmp/test1/sub/file'))
                .then(actual => assert.deepEqual(actual, expected));
        });
    });

});

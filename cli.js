#!/usr/bin/env node
const winston = require('winston');
const config = require('config');

winston.configure({
    transports: config.get('logger.transports')
});

const Guardian = require('./src/Guardian');

new Guardian().cli();


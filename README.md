# Guardians-Client
This project is part of the Guardians project, which is explained at [Guardians-Doc](https://framagit.org/guardians/guardians-doc).  
Guardians-Client takes care of your data, by watching each change made on every files you want, and automatically sending these to the [Guardians-Server](https://framagit.org/guardians/guardians-server).

# How to install
```shell
git clone https://framagit.org/guardians/guardians-client # Clone the project
npm install # Install dependencies
npm run init # Configure Client ; it will ask you few questions in order to work 
```

## A word about Client's initialization
When you start the `npm run init` command, it will ask few questions:
 * it configures server access. Please refer to [Guardians-Server](https://framagit.org/guardians/guardians-server) first.
 * it offers you to create a new User account.
User account allows you to manage all your data, even on different machines. You only should create a new User if this is the first Client you install. Otherwise, we recommend you to use the same User for all your machines.
 * it offers you to create a new Client.
A Client correspond to a computer. You should probably create a new Client for this computer, especially if this is the first time you install Guardians-Client on this machine.  
This Client will be added to your User account, so you'll be able to retrieve its files by using your User account.

Documentation about Users & Clients can be found on [Guardians documentation](https://framagit.org/guardians/guardians-doc).

# Configure paths to watch
The main purpose of Guardians-Client is to watch file changes.
You can easily watch your files by using the CLI:

``` shell
npm run cli
```
`help` and `usage` commands will show you everything you need to know about configuration:
 * `add <path> [--create|-c] [--update|-u] [--recursive|-r] [--delete|-d]`
 Mark a path as watched. `path` will be interpreted based on the project root folder, so we recommend to only use abolute paths to avoid interpretation problems.
   * `--create|-c` (default: false) send every created file to server.
   * `--update|-u` (default: false) send every change to server.
   * `--delete|-d` (default: false) if the file is deleted, ask the server to remove it too.
   * `--recursive|-r` (default: false) recusively watch changes

If you want to track each change of a particular folder on your disk, we recommand to use `add <absolutePathToFolder> -crud`.    
   
 
 * `update <path> [--create|-c] [--update|-u] [--recursive|-r] [--delete|-d]`
  Change path options. Use this command if you already watch a path but you want to change its options. Options are the same than the `add` command.
 
 * `rm <path>`
 Stop to watch  a path.

 * `list`
 List watched paths 
 
 * `restore <path>`
 Restore server version of `path` into your local Client.

# Start watching paths
As soon as you have configurated some paths to watch, you can start watching for file changes:
```Shell
npm run start
```
Guardians-Client will first tracking each changes made on watched paths since it stopped, before tracking configurated paths changes.

# What if I stop Guardians-Client ?
Guardians-Client handles changes made when it is stopped. These changes will be detected on startup.
It also manages server communication issues (such as server unreachable), so you can start & stop Client or connections as you want.

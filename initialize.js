const fs = require('q-io/fs');
const readlineSync = require('readline-sync');
const crypto = require("crypto");
const rewire = require('rewire');

const configOutput = './config/production.json';

class Initialize {
    constructor() {
        this.configOptions = {};
    }

    start() {
        return fs.exists(configOutput)
            .then(exists => {
                if (exists) {
                    console.error(`Project has already been initialized. Please remove '${configOutput}' configurations`);
                    process.exit(1);
                }
            })
            .then(() => fs.makeTree('./logs'))
            .then(this.createServerUrl.bind(this))
            .then(this.askForUser.bind(this))
            .then(this.askForClient.bind(this))
            .then(() => {
                fs.write(configOutput, JSON.stringify(this.configOptions))
            })
            .catch(err => {
                console.log('error:', err);
                process.exit(1);
            });
    }

    createServerUrl() {
        console.log(" # Server connection");
        this.configOptions.server = {
            url: readlineSync.question("Please provide server URL: "),
            strictSSL: readlineSync.keyInYN("Does the server have a Strict SSL ?")
        };
    }

    askForUser() {
        console.log(" # Client login");

        if (readlineSync.keyInYN('Do you already have a User account ?')) {
            this.user = {
                username: readlineSync.question("Please provide the User login: "),
                password: readlineSync.question("Please provide the User password: ", {
                    min: 6,
                    max: 128,
                    hideEchoBack: true,
                    mask: ''
                })
            }
        } else {
            return this.createNewUser();
        }
    }

    createNewUser() {
        return this.loginAsAdmin()
            .then(() => {
                this.user = {
                    username: readlineSync.question("Please provide the name of this new User: "),
                    password: readlineSync.questionNewPassword("Please provide the password of this new User: ", {
                        min: 6,
                        max: 128,
                        mask: ''
                    })
                };

                const UserRequester = require('./src/Requesters/UserRequester');
                return new UserRequester().add(this.user.username, this.user.password);
            });
    }

    askForClient() {
        console.log(" # Client login");

        if (readlineSync.keyInYN('Do you already have a Client for this machine ?')) {
            this.configOptions.credentials = {
                username: readlineSync.question("Please provide it name: "),
                password: readlineSync.question("Please provide it password: ", {
                    min: 6,
                    max: 128,
                    hideEchoBack: true,
                    mask: ''
                })
            };
            return Promise.resolve();
        }

        return this.createNewClient();
    }

    loginAsAdmin() {
        if (this.adminLogin) {
            return Promise.resolve();
        }

        console.log("\nFirst, you need to log as admin.");

        this.adminLogin = {
            username: readlineSync.question("Please provide the Admin username: "),
            password: readlineSync.question("Please provide the Admin password: ", {
                min: 6,
                max: 128,
                hideEchoBack: true,
                mask: ''
            })
        };

        // Write config file in order to use admin login as credentials
        this.configOptions.credentials = this.adminLogin;
        return fs.write(configOutput, JSON.stringify(this.configOptions))
    };

    createNewClient() {
        return this.loginAsAdmin()
            .then(() => {
                let clientName = readlineSync.question("Please provide the name of this new Client: ");
                let clientPassword = readlineSync.questionNewPassword("Please provide the password of this new Client: ", {
                    min: 6,
                    max: 128,
                    mask: ''
                });

                const ClientRequester = require('./src/Requesters/ClientRequester');
                return new ClientRequester().add(clientName, clientPassword, this.user)
                    .then(() => {
                        this.configOptions.credentials = {
                            username: clientName,
                            password: clientPassword
                        };
                    });
            });
    }
}


new Initialize().start();
const Guardian = require('./src/Guardian');
const winston = require('winston');
const config = require('config');

winston.configure({
    transports: config.get('logger.transports')
});

new Guardian().start();